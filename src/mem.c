#include "mem.h"


uint SIZE_MEMORY_TAB = 512;
object * MEMORY_TAB = NULL;

void debug_memory(){
  uint i = 0;
  uint count = 0;
  while(i<SIZE_MEMORY_TAB){
    if(MEMORY_TAB[i]!=NULL){
          count++;
          DEBUG_MSG("Pos : %d Type : %d Adresse : %p Ref_count : %d",i,MEMORY_TAB[i]->type,MEMORY_TAB[i],MEMORY_TAB[i]->ref_count);
      }
    i++;
  }
  DEBUG_MSG("COUNT : %d",count);
  return;
}

object * init_memory_tab(){
  object * mem_tab;
  mem_tab = sfs_calloc(SIZE_MEMORY_TAB,sizeof(void *));
  return mem_tab;
}
void clear_memory_tab(){
  uint i=0;
  while(i < SIZE_MEMORY_TAB){
    MEMORY_TAB[i]=NULL;
    i++;
  }
  return;
}
void * add_ptr_memory_tab(void * ptr){
  uint i=0;
  while(MEMORY_TAB[i]!=NULL && i < SIZE_MEMORY_TAB) i++;
  if( i >= SIZE_MEMORY_TAB){
    if(!resize()) return NULL;
  }
  MEMORY_TAB[i] = ptr;
  return ptr;
}

uint   sfs_garbage_collector(){
  uint count=0;
  uint total=0;
  uint i;
  do{
    i=0;
    count=0;
    while(i < SIZE_MEMORY_TAB){
      if(MEMORY_TAB[i]!=NULL && MEMORY_TAB[i]->ref_count <= 0){
        sfs_free_object(MEMORY_TAB[i]);
        count++;
      }
      i++;
      total += count;
    }
  }
  while(count !=0);
  return total;
}

void * resize(){
  object * NEW_MEM;
  SIZE_MEMORY_TAB = SIZE_MEMORY_TAB * 2;
  NEW_MEM=realloc(MEMORY_TAB,sizeof(object)*SIZE_MEMORY_TAB);
  if(!NEW_MEM) return NULL;
  MEMORY_TAB = NEW_MEM;
  return MEMORY_TAB;
}



void * sfs_malloc( size_t size ) {
  void * ptr = malloc(size);
  if(!ptr){
    ERROR_MSG("ERROR MALLOC : unable to allocate memory");
    return NULL;
  }
  return ptr;
}

void * sfs_calloc(size_t num, size_t size){
  void * ptr = calloc(num,size);
  if(!ptr){
    ERROR_MSG("ERROR CALLOC : unable to allocate memory");
    return NULL;
  }
  return ptr;
}

uint  sfs_free_object(object obj){
  if(obj==NULL) return 0;
  remove_ptr_memory_tab(obj);
  switch(obj->type){
    case SFS_STRING :
      if(obj->this.string != NULL) sfs_free(obj->this.string);
      sfs_free(obj);
      break;
    case SFS_SYMBOL :
      sfs_free(obj->this.symbol);
      sfs_free(obj);
      break;
    case SFS_PAIR :
      decrease_count(obj->this.pair.cdr);
      decrease_count(obj->this.pair.car);
      sfs_free(obj);
      break;
    case SFS_NORMAL_PAIR :
      decrease_count(obj->this.pair.cdr);
      decrease_count(obj->this.pair.car);
      sfs_free(obj);
      break;
    case SFS_PROCEDURE :
      decrease_count(obj->this.proc.formals);
      decrease_count(obj->this.proc.body);
      sfs_free(obj);
      break;
    case SFS_VARIABLE :
      /*
      decrease_count(obj->this.var.valeur);
      if(!obj->this.var.name) sfs_free(obj->this.var.name);
      sfs_free(obj);
      */
      decrease_count(obj->this.pair.cdr);
      decrease_count(obj->this.pair.car);
      sfs_free(obj);
      break;
    default :
      sfs_free(obj);
      break;
    }
    return 1;
}

void decrease_count(object obj){
  if(obj==NULL) return;
  obj->ref_count--;
  return;
}

void increase_count(object obj){
  if(obj==NULL) return;
  obj->ref_count++;
  return;
}

void  sfs_free( void *ptr ) {
    if(ptr==NULL) return;
    free( ptr );
    return;
}

void * remove_ptr_memory_tab(void * ptr){
  uint i = 0;
  while(i < SIZE_MEMORY_TAB){
    if(MEMORY_TAB[i]==ptr) MEMORY_TAB[i]=NULL;
    i++;
  }
  return ptr;
}
