#include <stdio.h>
#include <ctype.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "read.h"
#include "mem.h"

object isintoplevel(object symbol){
  /* recherche dans le niveau toplevel de la table de symbole si le symbole passé en argument existe
  on renvoit search_tabsymbol_environment */
  object toplevel=tab_symbol;
  while(toplevel->this.pair.car != nil) toplevel = toplevel->this.pair.car;
  return search_tabsymbol_environment(symbol,toplevel);
}

void flip( uint *i ) {

  if ( *i == FALSE ) {
    *i = TRUE;
  }
  else {
    *i = FALSE;
  }
}

/*
* @fn char* first_usefull_char(char* line)
*
* @brief retourne un pointeur sur le premier caractere utile dans line
* ou NULL si line ne contient que des espaces et des commentaires
*/
char* first_usefull_char(char* line) {

  int i=0;
  if (line == NULL) {
    return NULL;
  }
  i = 0;
  /* on saute les espaces */
  while(line[i] != '\0' && isspace(line[i])) {
    i++;
  }
  /* si fin de ligne => ligne inutile */
  if(line[i] == '\0') {
    return NULL;
  }
  /* si premier caractere non espace est ';' => ligne inutile */
  if(line[i] == ';') {
    return NULL;
  }
  return line + i; /* ligne utile */
}

/**
* @fn uint  sfs_get_sexpr( char *input, FILE *fp )
*
* @brief extrait la prochaine S-Expression dans le flux fp et la stocke dans input
* (meme si elle est repartie sur plusieurs lignes)
* @param fp (FILE *) flux d'entree (ouvert en ecriture, mode texte)
* @param input (char *) chaine allouee de taille BIGSTRING, dans laquelle la S-Expression sera stockée
*
* @return S_OK si une S-Expression apparemment valide a ete trouvee
* @return S_KO si on n'a pas trouve de S-Expression valide
* @return S_END si fin de fichier atteinte sans avoir lu de caractere utile.
*
* sfs_get_sexpr commence par lire une ligne dans fp,
* puis compte le nombre de parentheses ouvrantes et fermantes sur la ligne.
* Les parentheses dans des chaines et les caracteres Scheme #\( et #\)
* ne sont pas comptes.
*
* Si le compte devient zéro et que
*        - la ligne est fini, la fonction retourne S_OK
* 				- la ligne n'est pas fini la fonction retourne S_KO
*
* S'il y a plus de parentheses fermantes qu'ouvrantes,
* la fonction retourne S_KO.
* Les commentaires et espaces qui figurent a la fin de chaque ligne
* sont remplacés par un espace.
* Les espaces qui figurent a la fin de la S-Expression (y compris '\n')
* sont supprimés.
*
* Attention : cette fonction refuse les S-Expression multiple sur une seule ligne. Ainsi :
*    a b c
*    (qqchose) (autrechose)
*    (qqchose) 78
* seront considereees comme des erreurs
* et la fonction retournera S_KO
*
* @pre fp doit etre prealablement ouvert en lecture
* @pre input doit etre prealablement alloue en memoire, de taille BIGSTRING
*/

typedef enum {
  NOTHING,        /* rien n'a ete trouve encore.. */
  STRING_ATOME,   /* la premiere trouvee dans la ligne semble etre un atome */
  BASIC_ATOME,    /* la premiere trouvee dans la ligne semble etre d'une chaine */
  S_EXPR_PARENTHESIS, /* la premiere trouvee dans la ligne semble etre une expression parenthesee */
  FINISHED        /* on a trouve une S-Expr bien formee */
} EXPRESSION_TYPE_T;

uint  sfs_get_sexpr( char *input, FILE *fp ) {
    int       parlevel = 0;
    uint      in_string = FALSE;
    uint      s = 0;
    char      k[BIGSTRING];
    char      *ret = NULL;
    char      *chunk = NULL;
    uint      i = 0;
    string    sfs_prompt;

    EXPRESSION_TYPE_T typeOfExpressionFound = NOTHING;

    parlevel = 0;
    memset( input, '\0', BIGSTRING );

    do {
        ret = NULL;
        chunk = NULL;

        /* si en mode interactif*/
        if ( stdin == fp ) {
            uint nspaces = 2*parlevel;

            init_string( sfs_prompt );

            /* le prompt indique le niveau de parenthese
               et decale la prochaine entrée en fonction
               de ce niveau (un peu à la python)*/
            sprintf( sfs_prompt, "SFS:%u > ", parlevel );

            for ( i= 0; i< nspaces; i++ ) {
                sfs_prompt[strlen(sfs_prompt)] = ' ';
            }

            /* si sur plusieurs lignes, le \n équivaut à un espace*/
            if (nspaces>0) {
                input[strlen(input)+1] = '\0';
                input[strlen(input)] = ' ';
            }

            /*saisie de la prochaine ligne à ajouter dans l'input*/
            chunk = readline( sfs_prompt );
        }
        /*si en mode fichier*/
        else {
            chunk=k;
            memset( chunk, '\0', BIGSTRING );
            ret = fgets( chunk, BIGSTRING, fp );

            if ( NULL == ret ) {
                /* fin de fichier...*/
                if ( parlevel != 0 ) {
                    WARNING_MSG( "Parse error: missing ')'" );
                    return S_KO;
                }
                return S_END;
            }

            if (strlen(chunk) == BIGSTRING-1
                    && chunk[BIGSTRING-1] != '\n'
                    && !feof(fp)) {
                WARNING_MSG( "Too long line for this interpreter!" );
                return S_KO;
            }
        }

        /* si la ligne est inutile
        	=> on va directement à la prochaine iteration */
        if (first_usefull_char(chunk) == NULL) {
            continue;
        }


        s = strlen( chunk );

        if ( s > 0 ) {
            if (strlen(input) + s > BIGSTRING-1 ) {
                WARNING_MSG( "Too long a S-expression for this interpreter!" );
                return S_KO;
            }

            for ( i = 0; i< strlen(chunk); i++ ) {
                /* si la fin de la ligne chunk est inutile,
                   on ajoute un espace dans input et on sort de la boucle*/
                if ( in_string == FALSE && first_usefull_char(chunk + i) == NULL ) {
                    chunk[i]='\0';
                    input[strlen(input)] = ' ';
                    break;
                }


                switch(chunk[i]) {
                case '(':
                    if (in_string == FALSE
                            && ! ( i>1 && chunk[i-1] == '\\' && chunk[i-2] == '#' ) ) {
                        parlevel++;
                        typeOfExpressionFound = S_EXPR_PARENTHESIS;
                    }
                    break;
                case ')':
                    if ( in_string == FALSE
                            && ! ( i>1 && chunk[i-1] == '\\' && chunk[i-2] == '#' ) ) {
                        parlevel--;
                        if (parlevel == 0 ) {
                            typeOfExpressionFound = FINISHED;
                        }
                        if ( parlevel < 0 ) {
                            WARNING_MSG( "Parse error : cannot start with ')'" );
                            return S_KO;
                        }
                    }
                    break;
                case '"':
                    if ( i<2 || chunk[i-1] != '\\' ) {
                        if ( in_string == FALSE ) {
                            if(typeOfExpressionFound == BASIC_ATOME) {
                                WARNING_MSG("Parse error: invalid string after atom : '%s'", chunk+i);
                                return S_KO;
                            }
                            in_string = TRUE;
                            if(typeOfExpressionFound != S_EXPR_PARENTHESIS) {
                                typeOfExpressionFound = STRING_ATOME;
                            }
                        }
                        else {
                            in_string = FALSE;
                            if(typeOfExpressionFound == STRING_ATOME) {
                                typeOfExpressionFound = FINISHED;
                            }
                        }
                    }
                    break;
                default:
                    if(in_string == FALSE) {
                        if(isspace(chunk[i])) {
                            if(typeOfExpressionFound == BASIC_ATOME) {
                                typeOfExpressionFound = FINISHED;
                            }
                        } else if(typeOfExpressionFound != S_EXPR_PARENTHESIS) {
                            typeOfExpressionFound = BASIC_ATOME;
                        }
                    }
                    break;
                }


                if(typeOfExpressionFound == FINISHED) {
                    char *first_useful = first_usefull_char(chunk + i + 1);
                    if( first_useful != NULL) {
                        if(*first_useful == ')' ) {
                            WARNING_MSG( "Parse error: too many closing parenthesis')'" );
                        }
                        else {
                            WARNING_MSG("Parse error: invalid trailing chars after S-Expr : '%s'", chunk+i);
                        }
                        return S_KO;
                    }
                }

                /* recopie char par char*/
                input[strlen(input)] = chunk[i];
            }
            if(in_string == TRUE) {
                WARNING_MSG( "Parse error: non terminated string on line %s", chunk );
                return S_KO;
            }
        }


        if ( parlevel > 0 && fp != stdin ) {
            if ( feof( fp ) ) {
                WARNING_MSG( "Parse error: missing ')'" );
                return S_KO;
            }

            if (input[strlen(input)-1] == '\n') input[strlen(input)-1] = ' ';
        }
    } while ( parlevel > 0 );

    /* Suppression des espaces restant a la fin de l'expression, notamment le dernier '\n' */
    while (isspace(input[strlen(input)-1])) input[strlen(input)-1] = '\0';

    if(stdin == fp) {
        add_history( input );
    }
    return S_OK;
}

object sfs_read( char *input, uint *here ){
  while( input[*here] !='\0' &&  isspace( input[*here] )  && input[*here] != ')' && input[*here] != '(' ) (*here)++;
  if ( input[*here] == '(' ) {
    while( input[*here] !='\0' &&  isspace(input[*here] ) && input[*here] != ')' && input[*here] != '(' ) (*here)++;
      if ( input[(*here)+1] == ')' ) {
        *here += 2;
        return nil;
      }
      else {
        (*here)++;
        return sfs_read_pair( input, here );
      }
    }
    else {
      return sfs_read_atom( input, here );
    }
  return NULL;
}

object sfs_read_atom( char *input, uint *here ) {

      uint step = *here;

      while( input[step] !='\0' && isspace( input[step] ) && input[step] != ')' && input[step] != '(' ) (step)++;
      /* on se positionne au début de ce qui nous intéresse sachant que pour l'instant on a détecté que des espaces */

      if ( input[step] == '\0' ){ /*fin de l'atome */
      *here = &input[step] - input;
      WARNING_MSG("Atom non lu : EOF");
      return NULL;
    }

    if (input[step] == ')' || input[step] == '(' ){ /*fin de la pair*/
      *here = &input[step] - input;
      return nil;
    }
    else{
      if( isdigit(input[step]) ||
      ( input[step] == '.' && isdigit(input[step+1]) ) ||
      ( is_signed(input[step]) &&  ( isdigit(input[step+1]) || ( input[step+1] == 'i' && is_delimiter(input[step+2]) ) )  ) ){ /* début d'un nombre */
      /* Si c'est un plus ou un moins et qu'il est suivi par un nombre  ou par i >>>> OK
      Si ca commence par un nombre >>>> OK
      Si c'est un réel commencant par '.' comme .8 par exemple >>>>> OK
      Sinon ce n'est pas un nombre */

        *here = step;

        return sfs_read_number( input , here );
      }

      if(input[step] == '#' && input[step+1] != '\0' ){ /* début d'un char, d'un boolean ou d'un symbole */

      if( ( input[step+2] == '\0' || is_delimiter(input[step+2] ) ) && ( input[step+1] == 't' || input[step+1] == 'f' ) ) { /* boolean */
        *here = step;

        return sfs_read_boolean(input,here);
      }
      if( input[step+1] == '\\'){ /* char */

        *here = step;

        return sfs_read_char(input,here);
      }
      else{
        WARNING_MSG("boolean ou caractere non valide");
        (*here)++;
        return NULL;
      }

    }

    if(input[step] == '"' && input[step+1]!='\0'){ /* on lit une chaine de caractéres*/

      *here = step;

      return sfs_read_string(input,here);

    }
    else{/*Symbol*/

    *here = step;

    return sfs_read_symbol(input,here);
    }
  }
  WARNING_MSG("atom non reconnu ");
  return NULL;
}

uint number_type(char * input , uint * here){
  uint  step = *here;
  return type_complex(input, &step);
}


uint type_complex(char * input , uint * here ){
  /*les complexes sont de la forme REAL_PART'sign'IM_PARTi sans espaces entre les menbres ( REAL_PART, 'sign',IM_PART et i)
  et toujours dans cette ordre */
 uint type;
 type = type_real_complex(input,here);

 if(type == NUM_COMPLEX) return type; /* complexe pur */

 if(type == NUM_UINTEGER || type == NUM_INTEGER || type == NUM_REAL){ /* partie réelle */

   if(is_signed(input[*here])){

     if(type_real_complex(input,here) == NUM_COMPLEX) return NUM_COMPLEX; /* exemple 4+4i*/
     else return NUM_UNDEF;/* exemple : 4+4 */
   }
   else return type;
 }
 return NUM_UNDEF;
}

uint type_real_complex(char * input , uint * here ){

  uint type = NUM_UINTEGER; /* on part du cas le plus simple qu'est un nombre entier non signé*/

  if(is_signed(input[*here])){
    (*here)++;
    if(input[*here] == 'i'){
      return NUM_COMPLEX;
    } /* dans le cas ou on a +i , -i , 4-i , 4+i*/
    else type = NUM_INTEGER; /* si c'est signé + ou - on change le type en entier signé */
  }

  if(jumpdigit(input,here)) return type; /* digit avec EOF ou delimiteur à la fin donc on a bien un entier relatif
  ex: -4) , 5( , +7" ou 8'\0' */

  if(is_signed(input[*here])){
    return type;
  } /* le cas ou on capte le type de la partie réelle de 4+5i par exemple , (possibilité de  combiner avec le if précédent ) */

  if(input[*here] == 'i' && is_delimiter(input[*here+1])) return NUM_COMPLEX; /* exemple 5i  ,  +4i ,  -7i */

  if(input[*here] == '.'){
    (*here)++;
    if(jumpdigit(input,here)) return NUM_REAL; /* le cas ou on a 3.4  OU .7 */
    if(is_signed(input[*here])){
      return NUM_REAL;
    }  /* le cas ou on capte le type de la partie réelle de 3.4+5i par exemple */

    if(input[*here] == 'i' && is_delimiter(input[*here+1])) return NUM_COMPLEX; /* le cas ou on capte le type de la partie imaginaire de 5.4i */
  }
  /*si on ne trouve pas le type du nombre on renvoit NUM_UNDEF */
  return NUM_UNDEF;
}


uint is_signed(char input){
  if(input == '+' || input == '-') return 1;
  else return 0;
}

uint is_delimiter(char input){
  if(input == '\0' || isspace(input) || input == ')' || input == '(' || input == '"') return 1;
  else return 0;
}

uint jumpspace(char * input , uint * here){/* saute les espaces et renvoit 1 en cas de EOF ou de delimiteur */
  while(isspace(input[*here])) (*here)++;
  if(is_delimiter(input[*here]) ) return 1;
  else return 0;
}

uint jumpdigit(char * input , uint * here){/* saute les digits et renvoit 1 en cas de EOF ou de delimiteur */
  while(isdigit(input[*here])) (*here)++;
  if(is_delimiter(input[*here]) ) return 1;
  else return 0;
}

object sfs_read_number(char * input , uint * here ){

  char * endptr = input;
  num number;
  uint type;
  double value_error;
  type = number_type(input,here);
  if(type == NUM_UNDEF){
    (*here)++;
    jumpdigit(input,here);
    WARNING_MSG("Nombre non valide");
    return NULL;
  }

  if(type == NUM_INTEGER){
    number.numtype = NUM_INTEGER;
    number.this.integer = strtol( &input[*here] , &endptr , 10);
    *here = endptr - input;
  }

  if(type == NUM_UINTEGER){
    number.numtype = NUM_UINTEGER;
    number.this.uinteger = strtoul( &input[*here] , &endptr , 10);
    *here = endptr - input;
  }

  if(type == NUM_REAL){
    number.numtype = NUM_REAL;
    number.this.real = strtod( &input[*here] , &endptr);
    *here = endptr - input;
  }

  if(type == NUM_COMPLEX){
    number = sfs_read_number_complex(input,here);
  }


  /*nombre confirmé mais on regarde si il y a une erreur de plage de valeur */
  if( errno == ERANGE ){
    errno = 0;
    value_error = sfs_eval_number(number,REAL_PART);
    if( value_error >= 0) return make_number_pinfty();
    if( value_error < 0) return make_number_minfty();
  }
  return make_number(number);
}

num sfs_read_number_complex( char * input , uint * here ){
  num number;
  char * endptr = input;
  double value;
  number.numtype = NUM_COMPLEX;

  if(input[*here] == '+' && input[*here+1] == 'i' ){
    number.this.cplx.RP = 0;
    number.this.cplx.IP = 1;
    (*here) += 2;
    return number;
  }

  if(input[*here] == '-' && input[*here+1] == 'i' ){
    number.this.cplx.RP = 0;
    number.this.cplx.IP = -1;
    (*here) += 2;
    return number;
  }


  value = strtod( &input[*here] , &endptr);
  *here = endptr - input;
  if(input[*here] == 'i'){
    number.this.cplx.RP = 0;
    number.this.cplx.IP = value;
    (*here)++;
  }
  else{
    number.this.cplx.RP = value;
    value = strtod( &input[*here] , &endptr);
    if(value == 0 ){
      if(input[*here] == '-') number.this.cplx.IP = -1;
      if(input[*here] == '+') number.this.cplx.IP = +1;
      (*here) +=2;
    }
    else {
      number.this.cplx.IP = value;
      *here = endptr - input+1;
    }
  }


  if(number.this.cplx.IP == 0){
    number.numtype = NUM_REAL;
    number.this.real = number.this.cplx.RP;
    return number;
  }
  return number;
}

object sfs_read_boolean(char * input , uint * here){

  if(input[*here+1] == 't' ){

    *here = &input[*here +2] - input; /* mise à jour de la position*/

    return vrai; /*boolean vrai confirmé et renvoyé*/
  }

  if(input[*here+1] == 'f' ){

    *here = &input[*here +2] - input; /* mise à jour de la position */

    return faux; /*boolean faux confirmé et renvoyé*/
  }
  WARNING_MSG("boolean ou caractere non valide");
  return NULL;
}

object sfs_read_char( char * input , uint * here ){

  string buff;

  if(input[*here+1] == '\\'){

    if( isspace( input[*here+2] ) || input[*here+2] == '\0'){

      WARNING_MSG("boolean ou caractere non valide");

      return NULL;
    }

    if(input[*here+2]<=126 && input[*here+2] >=33 && input[*here+2] != '\\' && input[*here+2] != '"' &&  is_delimiter(input[*here+3])){


      /* char validé , c'est un charactère excepté \ et " */

      *here = *here + 3; /* on oubli pas de mettre à jour la position */

      return make_char(input[*here-1]);
    }

    if(input[*here+2] == '\\'){
      *here = *here + 3;
      return make_char('\\');
    }
    if(input[*here+2] == '"' ){
      *here = *here + 3;
      return make_char('"');
    }

    sscanf(&(input[*here +2]),"%s",buff);

    if(strcmp(buff,"space") ==0){
      *here = *here + 7;
      return make_char(' ');
    }
    if(strcmp(buff,"newline") ==0){
      *here = *here + 9;
      return make_char('\n');
    }
    /* char non reconnu */

    WARNING_MSG("boolean ou caractere non valide");
    (*here)++;
    return NULL;

  }

  WARNING_MSG("boolean ou caractere non valide");
  return NULL;
}


object sfs_read_symbol(char * input , uint * here ){

  char * bigstr = sfs_calloc(BIGSTRING,sizeof(char));
  object item;
  object function;
  uint length = 0;

  if(bigstr == NULL){
    ERROR_MSG("allocation sfs_read_symbol impossible");
    return NULL;
  }

  if(input[*here]=='\'') {
    (*here)++;
    sfs_free(bigstr);
    return sfs_read_quote_simplified(input,here);
  }

  sscanf(&(input[*here]),"%s",bigstr);

  if(strlen(bigstr) > BIGSTRING){
    ERROR_MSG("symbol trop long");
    return NULL;
  }

   /* à débugguer dans le cas ou on a par exemple (symbol1(symbol2)) qui capte symbol1(symbol2 au lieu de symbol1*/

  while(bigstr[length] != ')' && bigstr[length] !='(' && bigstr[length] !='\0') length++;

  bigstr[length]='\0';

  *here += length;

  item = make_symbol(bigstr);
  sfs_free(bigstr);

  if(length < 15){ /* permet d'éviter de chercher dans la table des symboles trop longs ou trop petits */
    function = isintoplevel(item);
    if(function != nil){
      sfs_free_object(item);
      item = function;
    }
  }
  return item;
}

object sfs_read_quote_simplified(char * input, uint * here){

  object obj_quote_cdr=make_pair(sfs_read(input,here),nil);
  object obj_quote=make_pair(quote_function,obj_quote_cdr);
  return obj_quote;
}

object sfs_read_pair( char *stream, uint *i ) {

  object pair = NULL;
  object paircar = NULL;
  object paircdr = NULL;

  while( stream[*i] !='\0' && isspace( stream[*i] ) && stream[*i] != ')' && stream[*i] != '(' ) (*i)++;
  if(stream[*i] == '\0'){
    WARNING_MSG("Syntaxe invalide : ) manquant ");
    return NULL;
  }

  if(stream[*i] == ')' ){
    (*i)++;
    return nil;
  }
  else{

    paircar = sfs_read(stream,i);
    jumpspace(stream,i);
    if(stream[*i] == '.' && isspace(stream[*i+1])){
      (*i)++;
      paircdr = sfs_read(stream,i);
      jumpspace(stream,i);
      if(stream[*i] == ')')(*i)++;
      pair = make_normal_pair(paircar,paircdr);
      return pair;
    }
    else{
      paircdr = sfs_read_pair(stream,i);
      pair = make_pair(paircar,paircdr);
      return pair;
    }
  }
}

object sfs_read_string( char *input, uint *here ){

  char * buff = sfs_calloc(BIGSTRING,sizeof(char));
  uint step = *here;
  object item;

  uint bstep = 0;
  char endstr = 0;

  step++;
  while( input[ step ] != '\0' && endstr != 1 ){
    if(bstep >= BIGSTRING -10){
      WARNING_MSG("ERREUR sfs_read_string : chaine de caractères trop grande");
      return NULL;
    }

    switch (input[step]){
      case '\\' :
      if(input[step+1] != '"' && input[step+1] != '\\' ){
        WARNING_MSG("mauvais usage de \\ ");
        return NULL;
      }
      if(input[step+1] == '\\'){
        buff[bstep] = '\\';
        step += 2;
        bstep++;
      }
      if(input[step+1] == '"'){
        buff[bstep] = '"';
        step += 2;
        bstep++;
      }
      break;
      case '"' :
      if( is_delimiter(input[step+1] ) ){
        buff[bstep] = '\0';
        step++;
        endstr = 1;
      }
      else{
        WARNING_MSG("mauvais usage de \" ");
        step++;
        *here=step;
        return NULL;
      }
      break;
      case '#' :
      if(input[step+1] == '\\'){
        if( isspace( input[step+2] ) || input[step+2] == '\0' ){

          WARNING_MSG("mauvais usage de #\\ ");
          step++;
          return NULL;
        }

        if(input[step+2]<=126 && input[step+2] >=33 && input[step+2] != '\\' && input[step+2] != '"' && ( isspace( input[step+3] ) || input[step+3] =='"' ) ){
          strncpy(&buff[bstep],&input[step],3);
          step = step + 3;
          bstep = bstep + 3;

        }

        if(input[step+2] == '\\'){
          strncpy(&buff[bstep],"#\\",2);
          step = step + 3;
          bstep = bstep + 2;

        }
        if(input[step+2] == '"' ){
          strncpy(&buff[bstep],"#\"",2);
          step = step + 3;
          bstep = bstep + 2;
        }

        if(strncmp(&input[step+2],"space", 5) ==0){
          strncpy(&buff[bstep],&input[step],7);
          step = step + 7;
          bstep = bstep + 7;
        }
        if(strncmp(&input[step+2],"newline",7) ==0){
          strncpy(&buff[bstep],&input[step],9);
          bstep = bstep + 9;
          step = step + 9;
        }

        break;
      }
      else{
        buff[bstep] = '#';
        bstep++;
        step++;
        break;
      }

      default :
      buff[bstep] = input[step];
      bstep++;
      step++;
      break;
    }
  }

  /*controle de la fin de la chaine de caractéres */

  if( buff[bstep] != '\0' ){
    WARNING_MSG("chaine de caractères non terminée par \" ");
    return NULL;
  }
  else{
    /* on se positionne juste aprés " */
    *here = step;
    item = make_string(buff);
    sfs_free(buff);
    return item;
  }
  return NULL;
}
