#include "print.h"

#include <stdio.h>

uint recursion = 0;

void sfs_print_atom( object o ) {
	switch ( o->type ){
		case SFS_NUMBER :
			switch(o->this.number.numtype){
				case NUM_REAL :
					printf("%f",o->this.number.this.real);
					break;
				case NUM_INTEGER :
					printf("%li",o->this.number.this.integer);
					break;
				case NUM_UINTEGER :
					printf("%li",o->this.number.this.uinteger);
				 	break;
				case NUM_COMPLEX :
					if(o->this.number.this.cplx.RP != 0) printf("%lf",o->this.number.this.cplx.RP);
					if(o->this.number.this.cplx.IP < 0){
						printf("%lfi",o->this.number.this.cplx.IP);
						break;
					}
					if(o->this.number.this.cplx.IP != 0)printf("+%lfi",o->this.number.this.cplx.IP);
					break ;
				case NUM_UNDEF :  break;
				case NUM_PINFTY :
					printf("+inf");
				 	break;
				case NUM_MINFTY :
					printf("-inf");
					break;
				default : break;
			}
			break;

		case SFS_CHARACTER :
			switch(o->this.character){
				case ' ' :
					printf("#\\space");
					break;
				case '\n' :
					printf("#\\newline");
					break;
				case '\"' :
					printf("#\\\"");
					break;
				default :
					printf("#\\%c",o->this.character);
					break;
			}
			break;

		case SFS_STRING :
			printf("\"%s\"",o->this.string);
			break;
		case SFS_BOOLEAN :
			if( o == vrai ) printf("#t");
			if( o == faux ) printf("#f");
			break;
		case SFS_SYMBOL :
			printf("%s",o->this.symbol);
			break;
		case SFS_FUNCTION :
				printf("#function:%s",o->this.fct.name);
				break;
		case SFS_PROCEDURE:
				printf("#procedure\n");
				break;
		case SFS_NORMAL_PAIR :
				recursion++;
				if(recursion > 1000){
					WARNING_MSG("Trop de recursion");
					return;
				}
				printf("( ");
				sfs_print_pair(o->this.pair.car);
				printf(" . ");
				sfs_print_pair(o->this.pair.cdr);
				printf(" )");
				break;
		case SFS_VARIABLE :
				printf("%s",o->this.pair.car->this.symbol);
				break;
		case SFS_NIL :
				printf("()");
				break;
	}
	return;
}

void sfs_print_pair( object o ) {

	if(o->type != SFS_PAIR){
		sfs_print_atom(o);
		return;
	}

	if(o->this.pair.car->type == SFS_PAIR ){
			printf("(");
			sfs_print_pair(o->this.pair.car);
			printf(")");
	}
	else sfs_print_atom(o->this.pair.car);



	if(o->this.pair.cdr->type == SFS_PAIR ){
		printf(" ");
		sfs_print_pair(o->this.pair.cdr);

	}
	else{
		if(o->this.pair.cdr->type != SFS_NIL ){
			printf(" ");
			sfs_print_atom(o->this.pair.cdr);
		}
	}

    return;
}

void sfs_print( object o ) {
	recursion = 0;
	if(o!=NULL){

		if ( SFS_PAIR == o->type ) {
			printf("(");
			sfs_print_pair( o );
			printf(")");
		}
		else {
			sfs_print_atom( o );
		}
	}
	else return;
}
