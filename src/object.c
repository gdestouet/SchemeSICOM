#include "object.h"
#include "mem.h"


object make_object( uint type ) {

	object t = sfs_malloc( sizeof( *t ) );

	t->type = type;
	t->ref_count = 0;
	return t;
}

object make_quit(void){
	object t = make_object(SFS_QUIT);
	t->this.special = t;
	return t;
}

object make_nil( void ) {

	object t = make_object( SFS_NIL );

	t->this.special = t;

	return t;
}

object make_boolean( void ){

	object t = make_object( SFS_BOOLEAN );

	t->this.special = t;

	return t;
}

num make_number_num(double real_part, double complex_part, uint type){
	num result;
	if(type == NUM_COMPLEX && complex_part == 0) type = NUM_REAL;
	result.numtype = type;
	switch (type){
		case NUM_REAL :
		result.this.real = real_part;
		break;
		case NUM_UINTEGER :
		result.this.uinteger = (unsigned long)real_part;
		break;
		case NUM_INTEGER :
		result.this.integer = (long)real_part;
		break;
		case NUM_COMPLEX :
		result.this.cplx.RP = real_part;
		result.this.cplx.IP = complex_part;
		break;
	}
	return result;
}

object make_number(num number){
	object t = make_object(SFS_NUMBER);

	t->this.number = number;
	add_ptr_memory_tab( t );
	return t;
}

object make_number_int( long int number ){

	object t = make_object( SFS_NUMBER );

	t->this.number.this.integer=number;
	t->this.number.numtype=NUM_INTEGER;
	add_ptr_memory_tab( t );
	return t;
}

object make_number_u_int( uint number ){

	object t = make_object( SFS_NUMBER );

	t->this.number.this.integer=number;
	t->this.number.numtype=NUM_UINTEGER;
	add_ptr_memory_tab( t );
	return t;
}

object make_number_float( double number){

	object t = make_object(SFS_NUMBER);

	t->this.number.this.real = number;
	t->this.number.numtype = NUM_REAL;
	add_ptr_memory_tab( t );
	return t;

}

object make_number_pinfty( void ){

	object t = make_object( SFS_NUMBER );

	t->this.number.this.integer=LONG_MAX;
	t->this.number.numtype=NUM_PINFTY;
	add_ptr_memory_tab( t );
	return t;
}

object make_number_minfty( void ){

	object t = make_object( SFS_NUMBER );

	t->this.number.this.integer=LONG_MIN;
	t->this.number.numtype=NUM_MINFTY;
	add_ptr_memory_tab( t );
	return t;
}

object make_char( char character ){

	object t = make_object( SFS_CHARACTER );

	t->this.character=character;
	add_ptr_memory_tab( t );
	return t;
}

object make_string( char *string ){

	object t = make_object( SFS_STRING );

	t->this.string = sfs_malloc(strlen(string)+1);
	strcpy(t->this.string,string);
	add_ptr_memory_tab( t );
	return t;
}

object make_procedure( object formals , object body ,object env){

	object t = make_object( SFS_PROCEDURE);
	increase_count(formals);
	increase_count(body);
	t->this.proc.formals = formals;
	t->this.proc.body = body;
	t->this.proc.env = env;
	add_ptr_memory_tab( t );
	return t;
}

object make_pair( object car, object cdr ){

	object t = make_object( SFS_PAIR );
	increase_count(car);
	increase_count(cdr);
	t->this.pair.car=car;
	t->this.pair.cdr=cdr;
	add_ptr_memory_tab( t );
	return t;
}

object make_normal_pair( object car, object cdr ){

	object t = make_object( SFS_NORMAL_PAIR);
	increase_count(car);
	increase_count(cdr);
	t->this.pair.car=car;
	t->this.pair.cdr=cdr;
	add_ptr_memory_tab( t );
	return t;
}

object make_symbol( char *symbol ){

	object t = make_object( SFS_SYMBOL );
	t->this.symbol = sfs_malloc(strlen(symbol)+1);
	strcpy(t->this.symbol,symbol);
	add_ptr_memory_tab( t );
	return t;
}

object make_variable( object symbol, object value){
	object var = make_pair(symbol,value);
	var->type = SFS_VARIABLE;
	return var;
}

object make_function( char * name, object (*ptr)() ){
	object t = make_object(SFS_FUNCTION);
	t->this.fct.name = sfs_malloc(strlen(name)+1);
	t->this.fct.ptr = ptr;
	strcpy(t->this.fct.name,name);
	return t;
}
