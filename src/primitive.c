#include "primitive.h"


uint SPECIAL_BINDING=0;

uint is_if(object eval_expr){
        if(eval_expr->type == SFS_PAIR){
            if(eval_expr->this.pair.car->type == SFS_FUNCTION && eval_expr->this.pair.car->this.fct.ptr == sfs_eval_if ) return 1;
            else return 0;
        }
        else return 0;
}

int sfs_eval_boolean(object obj){
    /* le type de l'object devrait être un SFS_BOOLEAN ou un SFS_NUMBER
      On peut implémenter l'évaluation d'un SFS_NUMBER :
      si il est nul on retourne FALSE ( 0 )
      sinon on retourne TRUE( 1 )

      pour l'instant ce fonctionnement n'est pas implémenté

      On met le cas si c'est une SFS_PAIR , comme ca on peut évaluer directement
      une opération booléenne ça evite de faire des distinctions de  dans sfs_eval_and ou sfs_eval_or

      */
      if(obj->type == SFS_PAIR){
        if(obj->this.pair.car->type == SFS_FUNCTION) obj = obj->this.pair.car->this.fct.ptr(obj->this.pair.cdr);
      }
      if(obj->type == SFS_SYMBOL){
        obj = sfs_eval(obj);
      }
      if(obj->type == SFS_BOOLEAN){
        if(obj == vrai) return 1;
        else return 0;
      }

      if(obj->type == SFS_NUMBER){
        if( sfs_eval_number(obj->this.number,REAL_PART) !=0 ) return 1;
        else return 0;
      }
      /* par défaut si l'évaluation échoue on renvoit -1 */
      WARNING_MSG("l'argument doit etre un nombre, un booleen ou une paire dont le car est un opérateur booleen");
      return -1;
}

double sfs_eval_number(num number,uint PART){
  /*permet de retourner la valeur d'un object de type SFS_NUMBER qu'il soit un flottant ou un int */
  if(number.numtype != NUM_COMPLEX && PART == COMPLEX_PART) return 0;
  if(number.numtype == NUM_REAL){
    return number.this.real;
  }

  if(number.numtype == NUM_INTEGER){
    return number.this.integer;
  }

  if(number.numtype == NUM_UINTEGER){
    return number.this.uinteger;
  }

  if(number.numtype == NUM_COMPLEX){
    if(PART == COMPLEX_PART) return number.this.cplx.IP;
    else return number.this.cplx.RP;
  }

  if(number.numtype == NUM_PINFTY){
    return LONG_MAX;
  }

  if(number.numtype == NUM_MINFTY){
    return LONG_MIN;
  }

  WARNING_MSG("evaluation du nombre non realise");
  return 0;
}

object sfs_eval_operator_add(object obj){

  object arg1;
  double value_real=0;
  double value_cplx=0;
  uint final_type=NUM_UINTEGER;
  num resultat = make_number_num(value_real,value_cplx,final_type);

  if(obj == nil) return make_number(resultat);
  if(obj->type != SFS_PAIR){
    WARNING_MSG("ERROR ADDITION");
    return NULL;
  }

  while(obj->type == SFS_PAIR){

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype == NUM_UNDEF){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }

    if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;

    value_real += sfs_eval_number(arg1->this.number,REAL_PART);
    if(arg1->this.number.numtype == NUM_COMPLEX) value_cplx += sfs_eval_number(arg1->this.number,COMPLEX_PART);
    obj = obj->this.pair.cdr;
  }
  resultat = make_number_num(value_real,value_cplx,final_type);
  return make_number(resultat);
}

  object (sfs_eval_operator_sub)(object obj){

    object arg1;
    double value_real=0;
    double value_cplx=0;
    uint final_type=NUM_UINTEGER;
    num resultat = make_number_num(value_real,value_cplx,final_type);

    if(obj == nil) return make_number(resultat);
    if(obj->type != SFS_PAIR){
      WARNING_MSG("ERROR SUBSTRACTION");
      return NULL;
    }

    if(obj->this.pair.cdr->type == SFS_PAIR){
      arg1 = obj->this.pair.car;
      arg1 = sfs_eval(arg1);
      if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;
      value_real = sfs_eval_number(arg1->this.number,REAL_PART);
      value_cplx = sfs_eval_number(arg1->this.number,COMPLEX_PART);
      obj = obj->this.pair.cdr;
    }

    while(obj->type == SFS_PAIR){

      arg1 = obj->this.pair.car;
      arg1 = sfs_eval(arg1);

      if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype == NUM_UNDEF){
        WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
        return NULL;
      }

      if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;

      value_real -= sfs_eval_number(arg1->this.number,REAL_PART);
      if(arg1->this.number.numtype == NUM_COMPLEX) value_cplx -= sfs_eval_number(arg1->this.number,COMPLEX_PART);
      obj = obj->this.pair.cdr;
    }
    resultat = make_number_num(value_real,value_cplx,final_type);
    return make_number(resultat);
}

object sfs_eval_operator_multip(object obj){

    object arg1;
    double value_real=1;
    double value_cplx=0;
    double buff;
    double real2;
    double im2;
    uint final_type=NUM_UINTEGER;
    num resultat = make_number_num(value_real,value_cplx,final_type);

    if(obj == nil) return make_number(resultat);
    if(obj->type != SFS_PAIR){
      WARNING_MSG("ERROR MULTIPLICATION");
      return NULL;
    }

    while(obj->type == SFS_PAIR){

      arg1 = obj->this.pair.car;
      arg1 = sfs_eval(arg1);

      if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype == NUM_UNDEF){
        WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
        return NULL;
      }
      if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;
      if(arg1->this.number.numtype == NUM_COMPLEX){
        buff = value_real;
        real2 = sfs_eval_number(arg1->this.number,REAL_PART);
        im2 = sfs_eval_number(arg1->this.number,COMPLEX_PART);
        value_real =  value_real*real2 -
                      value_cplx*im2;
        value_cplx = im2*buff + value_cplx*real2;
      }
      else{
        buff = sfs_eval_number(arg1->this.number,REAL_PART);
        value_real *= buff;
        if(value_cplx) value_cplx *= buff;
      }
      obj = obj->this.pair.cdr;
    }
    resultat = make_number_num(value_real,value_cplx,final_type);
    return make_number(resultat);
}

object sfs_eval_operator_divid(object obj){

      object arg1;
      double value_real=1;
      double value_cplx=0;
      double buff;
      double real2;
      double im2;
      uint final_type = NUM_UINTEGER;
      num resultat = make_number_num(value_real,value_cplx,final_type);
      if(obj == nil) return make_number(resultat);
      if(obj->type != SFS_PAIR){
        WARNING_MSG("ERROR DIVISION");
        return NULL;
      }
      /* Si on a un seul element on retourne son inverse , s'il y a plus d'un argument on retourne le premier divisé par les autres arguments
      ex : ( / z1 z2 z3) ==> z1/(z2*z3) */
      if(obj->this.pair.cdr->type == SFS_PAIR){
        arg1 = obj->this.pair.car;
        arg1 = sfs_eval(arg1);
        if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;
        value_real = sfs_eval_number(arg1->this.number,REAL_PART);
        value_cplx = sfs_eval_number(arg1->this.number,COMPLEX_PART);
        obj = obj->this.pair.cdr;
      }

      while(obj->type == SFS_PAIR){

        arg1 = obj->this.pair.car;
        arg1 = sfs_eval(arg1);

        if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype == NUM_UNDEF){
          WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
          return NULL;
        }
        if(arg1->this.number.numtype > final_type) final_type = arg1->this.number.numtype;
        if(arg1->this.number.numtype == NUM_COMPLEX){
          buff = value_real;
          real2 = sfs_eval_number(arg1->this.number,REAL_PART);
          im2 = sfs_eval_number(arg1->this.number,COMPLEX_PART);
          value_real =  value_real*real2 +
                        value_cplx*im2;
          value_cplx =  value_cplx*real2 - im2*buff;
          buff = real2*real2 + im2*im2;
          value_real = value_real / buff;
          value_cplx = value_cplx / buff;
        }
        else{
            buff = sfs_eval_number(arg1->this.number,REAL_PART);
            if(buff == 0) return make_number_pinfty(); /* temporaire  distinction de cas numerateur positif et negatif*/
            value_real = value_real / buff ;
            if(value_cplx) value_cplx = value_cplx / buff ;
        }
        obj = obj->this.pair.cdr;
      }
      resultat = make_number_num(value_real,value_cplx,final_type);
      return make_number(resultat);
}

object sfs_eval_operator_remainder(object obj){
  object arg1;
  long resultat1;
  long resultat2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car == nil ||
    obj->this.pair.cdr->this.pair.cdr != nil){
    WARNING_MSG("ERREUR REMAINDER : nécessite deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype > NUM_INTEGER){
    WARNING_MSG("ERREUR REMAINDER : Les arguments doivent être des entiers");
    return NULL;
  }

  resultat1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;
  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype > NUM_INTEGER){
    WARNING_MSG("ERREUR REMAINDER : Les arguments doivent être des entiers");
    return NULL;
  }

  resultat2 = sfs_eval_number(arg1->this.number,REAL_PART);
  resultat1 = resultat1 % resultat2;
  return make_number_int(resultat1);
}

object sfs_eval_operator_modulo(object obj){
  object arg1;
  long resultat1;
  long resultat2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car == nil ||
    obj->this.pair.cdr->this.pair.cdr != nil){
    WARNING_MSG("ERREUR MODULO : nécessite deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype > NUM_INTEGER){
    WARNING_MSG("ERREUR MODULO : Les arguments doivent être des entiers");
    return NULL;
  }

  resultat1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;
  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER || arg1->this.number.numtype > NUM_INTEGER){
    WARNING_MSG("ERREUR MODULO : Les arguments doivent être des entiers");
    return NULL;
  }

  resultat2 = sfs_eval_number(arg1->this.number,REAL_PART);
  if(resultat1 > 0 && resultat2 < 0) resultat1 = resultat1 % resultat2 - resultat2;
  if(resultat1 < 0 && resultat2 > 0) resultat1 = resultat1 % resultat2 + resultat2;
  else resultat1 = resultat1 % resultat2;
  return make_number_int(resultat1);
}

object sfs_eval_eqv(object obj){
  object arg1;
  object arg2;
  uint type_object;
  uint type_num;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr->type != SFS_PAIR ||
  obj->this.pair.cdr->this.pair.cdr != nil){
    WARNING_MSG("ERROR EQV? : cette fonction admet deux arguments");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  arg2 = sfs_eval(obj->this.pair.cdr->this.pair.car);
  if(!arg1 || !arg2) return NULL;
  if(arg1 == arg2) return vrai;
  type_object = arg1->type;
  if(type_object != arg2->type) return faux;
  switch (type_object) {
    case SFS_BOOLEAN:
      if(arg1 == arg2) return vrai;
      else return faux;
      break;
    case SFS_SYMBOL:
      if(strcpy(arg1->this.symbol,arg2->this.symbol)) return vrai;
      else return faux;
      break;
    case SFS_NUMBER:
      type_num = arg1->this.number.numtype;
      if(arg2->this.number.numtype != type_num) return faux;
      if(sfs_eval_number(arg2->this.number,REAL_PART) == sfs_eval_number(arg1->this.number,REAL_PART) &&
    sfs_eval_number(arg2->this.number,COMPLEX_PART) == sfs_eval_number(arg1->this.number,COMPLEX_PART)) return vrai;
      else return faux;
      break;
    case SFS_CHARACTER:
      if(arg1->this.character == arg2->this.character) return vrai;
      else return faux;
      break;
    case SFS_NIL :
      return vrai;
      break;
    case SFS_FUNCTION :
      if(arg1 != arg2 ) return faux;
      break;
  }
  return NULL;
}

object sfs_eval_boolean_operator_equal(object obj){

    object arg1;
    double eval1;
    double eval2;

    if(obj->type != SFS_PAIR ||
      obj->this.pair.car == nil ||
      obj->this.pair.cdr->type != SFS_PAIR ||
      obj->this.pair.cdr->this.pair.car ==nil){
      WARNING_MSG("ERREUR OPERATION : les opérateurs + - * / nécessite au moins deux arguments ");
      return NULL;
    }

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }

    eval1 = sfs_eval_number(arg1->this.number,REAL_PART);

    obj = obj->this.pair.cdr;

    while(obj->type == SFS_PAIR){

      arg1 = obj->this.pair.car;
      arg1 = sfs_eval(arg1);

      if(arg1 == NULL || arg1->type != SFS_NUMBER){
        WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
        return NULL;
      }
      eval2 = sfs_eval_number(arg1->this.number,REAL_PART);
      if(eval1 != eval2) return faux;

      eval1 = eval2;
      obj = obj->this.pair.cdr;
    }
    return vrai;
}

object sfs_eval_boolean_operator_inf(object obj){
  object arg1;
  double eval1;
  double eval2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car ==nil){
    WARNING_MSG("ERREUR OPERATION : les opérateurs + - * / nécessite au moins deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER){
    WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
    return NULL;
  }

  eval1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }
    eval2 = sfs_eval_number(arg1->this.number,REAL_PART);
    if(!(eval1 < eval2)) return faux;

    eval1 = eval2;
    obj = obj->this.pair.cdr;
  }
  return vrai;
}

object sfs_eval_boolean_operator_infeq(object obj){
  object arg1;
  double eval1;
  double eval2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car ==nil){
    WARNING_MSG("ERREUR OPERATION : les opérateurs + - * / nécessite au moins deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER){
    WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
    return NULL;
  }

  eval1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }
    eval2 = sfs_eval_number(arg1->this.number,REAL_PART);
    if(!(eval1 <= eval2)) return faux;

    eval1 = eval2;
    obj = obj->this.pair.cdr;
  }
  return vrai;
}

object sfs_eval_boolean_operator_sup(object obj){
  object arg1;
  double eval1;
  double eval2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car ==nil){
    WARNING_MSG("ERREUR OPERATION : les opérateurs + - * / nécessite au moins deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER){
    WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
    return NULL;
  }

  eval1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }
    eval2 = sfs_eval_number(arg1->this.number,REAL_PART);
    if(!(eval1 > eval2)) return faux;

    eval1 = eval2;
    obj = obj->this.pair.cdr;
  }
  return vrai;
}

object sfs_eval_boolean_operator_supeq(object obj){
  object arg1;
  double eval1;
  double eval2;

  if(obj->type != SFS_PAIR ||
    obj->this.pair.car == nil ||
    obj->this.pair.cdr->type != SFS_PAIR ||
    obj->this.pair.cdr->this.pair.car ==nil){
    WARNING_MSG("ERREUR OPERATION : les opérateurs + - * / nécessite au moins deux arguments ");
    return NULL;
  }

  arg1 = obj->this.pair.car;
  arg1 = sfs_eval(arg1);

  if(arg1 == NULL || arg1->type != SFS_NUMBER){
    WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
    return NULL;
  }

  eval1 = sfs_eval_number(arg1->this.number,REAL_PART);

  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){

    arg1 = obj->this.pair.car;
    arg1 = sfs_eval(arg1);

    if(arg1 == NULL || arg1->type != SFS_NUMBER){
      WARNING_MSG("ERREUR OPERATION : Les arguments doivent être des nombres");
      return NULL;
    }
    eval2 = sfs_eval_number(arg1->this.number,REAL_PART);
    if(!(eval1 >= eval2)) return faux;

    eval1 = eval2;
    obj = obj->this.pair.cdr;
  }
  return vrai;
}

object sfs_eval_and(object obj){
  object predi1;
  uint bool_val1;
  uint bool_val2;

  if(obj->type != SFS_PAIR || obj->this.pair.cdr->type != SFS_PAIR){
    WARNING_MSG("l'operateur and necessite au moins deux arguments");
    return NULL;
  }

  predi1 = obj->this.pair.car;
  bool_val1 = sfs_eval_boolean(predi1);
  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){
    predi1 = obj->this.pair.car;
    bool_val2 =sfs_eval_boolean(predi1);

    if(bool_val1 == -1 || bool_val2 == -1)  return NULL;

    if(!(bool_val1 && bool_val2)) return faux;
    bool_val1 = bool_val2;
    obj = obj->this.pair.cdr;

  }
  return vrai;
}

object sfs_eval_or(object obj){
  object predi1;
  uint bool_val1;
  uint bool_val2;

  if(obj->type != SFS_PAIR || obj->this.pair.cdr->type != SFS_PAIR){
    WARNING_MSG("l'operateur and necessite au moins deux arguments");
    return NULL;
  }

  predi1 = obj->this.pair.car;
  bool_val1 = sfs_eval_boolean(predi1);
  obj = obj->this.pair.cdr;

  while(obj->type == SFS_PAIR){
    predi1 = obj->this.pair.car;
    bool_val2 =sfs_eval_boolean(predi1);

    if(bool_val1 == -1 || bool_val2 == -1)  return NULL;

    if(bool_val1 || bool_val2) return vrai;
    bool_val1 = bool_val2;
    obj = obj->this.pair.cdr;
  }

  return vrai;
}

object sfs_eval_not( object obj){
    if(obj->this.pair.cdr != nil){
      WARNING_MSG("Erreur : not : trop d'arguments");
      return NULL;
    }
    if(!sfs_eval_boolean(obj->this.pair.car)) return vrai;
    else return faux;
}

object sfs_eval_ask_null(object obj){
  object arg;
  if(obj == nil) return vrai;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("NULL? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg == nil) return vrai;
  else return faux;
}

object sfs_eval_ask_boolean(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("boolean? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg == vrai || arg == faux) return vrai;
  else return faux;
}

object sfs_eval_ask_symbol(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type == SFS_SYMBOL) return vrai;
  else return faux;
}

object sfs_eval_ask_integer(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type != SFS_NUMBER) return faux; /* A tester avec (= obj (round obj)) */
  if(arg->this.number.numtype == NUM_INTEGER || arg->this.number.numtype == NUM_UINTEGER) return vrai;
  else return faux;
}

object sfs_eval_ask_complex(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type == SFS_NUMBER && arg->this.number.numtype != NUM_UNDEF) return vrai; /*tout les types de nombres de cet interpreteur sont au moins des complexes */
  else return faux;
}

object sfs_eval_ask_char(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type == SFS_CHARACTER) return vrai;
  else return faux;
}

object sfs_eval_ask_real(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type != SFS_NUMBER) return faux;
  if(arg->this.number.numtype >= NUM_UINTEGER && arg->this.number.numtype <= NUM_REAL) return vrai;
  else return faux;
}

object sfs_eval_ask_string(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type == SFS_STRING) return vrai;
  else return faux;
}

object sfs_eval_ask_pair(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("symbol? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg->type == SFS_NORMAL_PAIR || arg->type == SFS_PAIR) return vrai;
  else return faux;
}

object sfs_eval_ask_list(object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("list? prend 0 ou 1 argument");
    return NULL;
  }
  arg =sfs_eval(obj->this.pair.car);
  if(arg == NULL) return NULL;
  if(arg == nil || arg->type == SFS_PAIR) return vrai;
  else return faux;
}

object sfs_eval_ask_procedure( object obj){
  object arg;
  if(obj == nil) return faux;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr != nil){
    WARNING_MSG("procedure? prend 0 ou 1 argument");
    return NULL;
  }
  arg = sfs_eval(obj->this.pair.car);
  if(arg==NULL) return NULL;
  if(arg->type == SFS_PROCEDURE) return vrai;
  else return faux;
}

object sfs_eval_char_to_int(object obj){
  num integer;
  object arg1;
  if(obj->type != SFS_PAIR || obj == nil || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR CHAR->INT : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_CHARACTER){
    WARNING_MSG("ERROR CHAR->INT : l'argument doit etre un character");
    return NULL;
  }
  integer = make_number_num(arg1->this.character, 0 , NUM_INTEGER);
  return make_number(integer);
}

object sfs_eval_int_to_char(object obj){
  char c;
  object arg1;
  if(obj->type != SFS_PAIR || obj == nil || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR INT->CHAR : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_NUMBER && arg1->this.number.numtype != NUM_INTEGER &&
    arg1->this.number.numtype != NUM_UINTEGER){
    WARNING_MSG("ERROR INT->CHAR : l'argument doit etre un nombre entier");
    return NULL;
  }
  c = (char)sfs_eval_number(arg1->this.number,REAL_PART);
  return make_char(c);
}

object sfs_eval_let_star( object obj){
  SPECIAL_BINDING = 1;
  return sfs_eval_let(obj);
}

object sfs_eval_let (  object obj ){
  object binding;
  object var;
  object init;
  object bodylet;
  object proc;
  object bodyproc;
  object formalsproc=make_pair(nil,nil);
  object top_formalsproc=formalsproc;
  object valueproc=make_pair(nil,nil);
  object top_valueproc=valueproc;
  binding=obj->this.pair.car;
  bodylet=obj->this.pair.cdr;

  if(bodylet->type != SFS_PAIR){
   WARNING_MSG( "Mauvaise utilisation de let" );
   return NULL;
  }

 if( binding->type != SFS_PAIR ){
	  WARNING_MSG( "Mauvaise utilisation de let" );
    return NULL;
 }
 else{
    while( binding != nil ){
		   if( binding->type != SFS_PAIR ||
         binding->this.pair.car->type != SFS_PAIR ||
         binding->this.pair.car->this.pair.cdr->type != SFS_PAIR ||
         binding->this.pair.car->this.pair.cdr->this.pair.cdr != nil ){
           WARNING_MSG( "Mauvaise utilisation de let" );
           return NULL;
         }
		   else {
			    var=binding->this.pair.car->this.pair.car;
			    init=binding->this.pair.car->this.pair.cdr->this.pair.car;
			    binding=binding->this.pair.cdr;

          formalsproc->this.pair.car = var;
          valueproc->this.pair.car = init;
          increase_count(var);
          increase_count(init);
          if(binding!=nil){

            formalsproc->this.pair.cdr=make_pair(nil,nil);
            increase_count(formalsproc->this.pair.cdr);
            formalsproc=formalsproc->this.pair.cdr;
            valueproc->this.pair.cdr=make_pair(nil,nil);
            increase_count(valueproc->this.pair.cdr);
            valueproc=valueproc->this.pair.cdr;
          }
		   }
	  }
  }


  bodyproc=bodylet;
  proc=make_procedure(top_formalsproc,bodyproc,tab_symbol);
  return sfs_eval_procedure(proc,top_valueproc);
}

object sfs_eval_number_to_str(object obj){
  char buff[50];
  int n;
  object arg1;
  if( obj == nil || obj->type != SFS_PAIR  || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR NUMBER->STR : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_NUMBER){
    WARNING_MSG("ERROR NUMBER->STR : l'argument doit etre un nombre ");
    return NULL;
  }
  if(arg1->this.number.numtype == NUM_INTEGER || arg1->this.number.numtype == NUM_UINTEGER){
    n=sprintf(buff,"%li",(long)sfs_eval_number(arg1->this.number,REAL_PART));
  }
  if(arg1->this.number.numtype == NUM_REAL){
    n=sprintf(buff,"%lf",sfs_eval_number(arg1->this.number,REAL_PART));
  }
  if(arg1->this.number.numtype == NUM_COMPLEX){
    if(arg1->this.number.this.cplx.IP <0){
      n=sprintf(buff,"%lf%lfi",sfs_eval_number(arg1->this.number,REAL_PART), sfs_eval_number(arg1->this.number,COMPLEX_PART));
    }
    else{
      if(arg1->this.number.this.cplx.RP == 0) n=sprintf(buff,"%lfi", sfs_eval_number(arg1->this.number,COMPLEX_PART));
      else n=sprintf(buff,"%lf+%lfi",sfs_eval_number(arg1->this.number,REAL_PART), sfs_eval_number(arg1->this.number,COMPLEX_PART));
    }
  }
  if(n<0){
    WARNING_MSG("ERROR NUMBER->STR : le nombre est trop grand");
    return NULL;
  }
  else return make_string(buff);
}

object sfs_eval_str_to_number(object obj){
  object arg1;
  char * str;
  uint i=0;
  if(obj->type != SFS_PAIR || obj == nil || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR STR->NUMBER : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_STRING){
    WARNING_MSG("ERROR STR->NUMBER : l'argument doit etre une chaine de caractere ");
    return NULL;
  }
  str = arg1->this.string;
  if( isdigit(str[i]) ||
  ( str[i] == '.' && isdigit(str[i+1]) ) ||
  ( is_signed(str[i]) &&  ( isdigit(str[i+1]) || ( str[i+1] == 'i' && is_delimiter(str[i+2]) ) ) ) ) return sfs_read_number(str,&i);
  else return nil;
}

object sfs_eval_symbol_to_str(object obj){
  object arg1;
  char * str;
  if(obj->type != SFS_PAIR || obj == nil || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR SYMBOL->STR : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_SYMBOL){
    WARNING_MSG("ERROR SYMBOL->STR : l'argument doit etre un symbole ");
    return NULL;
  }
  str = calloc( strlen( arg1->this.symbol ) + 1 , sizeof( char ) );
  strcpy( str , arg1->this.symbol );
  return make_string(str);
}

object sfs_eval_str_to_symbol(object obj){
  object arg1;
  char * symbol;
  if(obj->type != SFS_PAIR || obj == nil || obj->this.pair.cdr != nil){
    WARNING_MSG("ERROR STR->SYMBOL : cette fonction n'accepte qu'un seul argument");
    return NULL;
  }
  arg1 = sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;
  if(arg1->type != SFS_STRING){
    WARNING_MSG("ERROR SYMBOL->STR : l'argument doit etre une chaine de caracteres ");
    return NULL;
  }
  symbol=calloc(strlen(arg1->this.string)+1,sizeof(char));
  strcpy(symbol,arg1->this.string);
  return make_symbol(symbol);
}

object sfs_eval_quote( object obj ){
  /*PROBABLEMENT UNE SOLUTION TEMPORAIRE*/
  if(obj == nil) return nil;
  if(obj->this.pair.cdr == nil) return obj->this.pair.car;
  else return obj;
}

object sfs_eval_define(object obj){

  object variable;
  object valeur;
  object procedure;
  object formals;
  object body;
  if(obj->type!=SFS_PAIR){
    WARNING_MSG("Mauvaise utilisation de define");
    return NULL;
  }

  if(obj->this.pair.car->type == SFS_PAIR){
    formals=obj->this.pair.car->this.pair.cdr;
    body = obj->this.pair.cdr;
    procedure = sfs_eval_lambda(make_pair(formals,body));
    if(procedure == NULL) return NULL;

    variable = obj->this.pair.car->this.pair.car;

    decrease_count(obj->this.pair.car);
    obj->this.pair.car = variable;
    increase_count(variable);

    decrease_count(obj->this.pair.cdr);
    obj->this.pair.cdr=make_pair(procedure,nil);
    increase_count(obj->this.pair.cdr);
  }

  if(obj->this.pair.car->type==SFS_FUNCTION){
       WARNING_MSG("ERREUR DEFINE : Impossible d'associer une valeur a ' %s ' , le nom est reserve",obj->this.pair.car->this.fct.name);
       return NULL;
  }


  if(obj->this.pair.car->type==SFS_SYMBOL){

    if(obj->this.pair.cdr->this.pair.car->type==SFS_STRING ||
       obj->this.pair.cdr->this.pair.car->type==SFS_CHARACTER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NUMBER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NORMAL_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PROCEDURE ||
       obj->this.pair.cdr->this.pair.car->type==SFS_FUNCTION ||
       obj->this.pair.cdr->this.pair.car->type==SFS_BOOLEAN ||
       obj->this.pair.cdr->this.pair.car->type==SFS_SYMBOL){

         variable=search_tabsymbol_environment(obj->this.pair.car,tab_symbol);

         valeur = obj->this.pair.cdr->this.pair.car; /*par défaut on prend le car du cdr commm étant la valeur */

         if(valeur->type==SFS_SYMBOL){ /* si c'est un symbol on cherche s'il est definit dans la table */

           valeur = search_tabsymbol(valeur,tab_symbol);

           if(valeur->type != SFS_VARIABLE){
             WARNING_MSG("ERREUR DEFINE : la variable ' %s ' n'est pas definie",obj->this.pair.cdr->this.pair.car->this.symbol);
             return NULL;
           }
           else valeur = valeur->this.pair.cdr;
         }
         else{
           if(valeur->type==SFS_PAIR) valeur = sfs_eval(valeur); /* si c'est une paire on l'évalue */
           if(valeur == NULL) return NULL;
         }
         if(variable==nil){ /* Si la variable n'est pas crée dans l'environnement courant on la définit */

          variable=make_variable(obj->this.pair.car,valeur);
          add_object_tabsymbol(variable,tab_symbol);
          return nil;
          }
          else { /* Si la variable est déjà définit on supprime l'ancienne valeur et on la remplace par la nouvelle */
            decrease_count(variable->this.pair.cdr);
            increase_count(valeur);
            variable->this.pair.cdr=valeur;
            return nil;
          }
    }
    else{
      WARNING_MSG("Mauvais type de donnee");
      return NULL;
    }
  }
  else{
    WARNING_MSG("Le nom de la variable doit etre un symbol");
    return NULL;
  }
return NULL;
}

object sfs_eval_set(object obj){
  object variable;
  object valeur;
  if(obj->type!=SFS_PAIR || obj->this.pair.cdr->type!=SFS_PAIR ||
     obj->this.pair.cdr->this.pair.cdr!=nil){
    WARNING_MSG("Mauvaise utilisation de set!");
    return NULL;
  }

  if(obj->this.pair.car->type==SFS_FUNCTION){
       WARNING_MSG("ERREUR SET! : Impossible d'associer une valeur a ' %s ' , le nom est reserve",obj->this.pair.car->this.fct.name);
       return NULL;
  }

  if(obj->this.pair.car->type==SFS_SYMBOL){
    if(obj->this.pair.cdr->this.pair.car->type==SFS_STRING ||
       obj->this.pair.cdr->this.pair.car->type==SFS_CHARACTER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NUMBER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NORMAL_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PROCEDURE ||
       obj->this.pair.cdr->this.pair.car->type==SFS_FUNCTION ||
       obj->this.pair.cdr->this.pair.car->type==SFS_BOOLEAN ||
       obj->this.pair.cdr->this.pair.car->type==SFS_SYMBOL){
        variable=search_tabsymbol(obj->this.pair.car,tab_symbol);

        valeur = obj->this.pair.cdr->this.pair.car; /*par défaut on prend le car du cdr commm étant la valeur */

        if(valeur->type==SFS_SYMBOL){ /* si c'est un symbol on cherche sa valeur dans la table */

          valeur = search_tabsymbol(valeur,tab_symbol);

          if(valeur->type == SFS_VARIABLE) valeur = valeur->this.pair.cdr;
            else{
              WARNING_MSG("ERREUR SET! : la variable ' %s ' n'est pas definie",obj->this.pair.cdr->this.pair.car->this.symbol);
              return NULL;
            }
        }

        if(valeur->type==SFS_PAIR) valeur = sfs_eval(valeur); /* si c'est une paire on l'évalue */
        if(valeur == NULL) return NULL;
        if(variable==nil){ /* Si la variable n'est pas crée dans n'importe quel environnement mauvaise utilisation de set */
          WARNING_MSG("Mauvaise utilisation de set!: la variable n'existe dans aucun environnement");
          return NULL;
        }
        if(variable==NULL) return variable;
        else { /* Si la variable est déjà définit on supprime l'ancienne valeur et on la remplace par la nouvelle */
          decrease_count(variable->this.pair.cdr);
          increase_count(valeur);
          variable->this.pair.cdr=valeur;
          return nil;
        }
    }
    else{
      WARNING_MSG("Mauvais type de donnée");
      return NULL;
    }
  }
  else{
    WARNING_MSG("Le nom de la variable doit être un symbole");
    return NULL;
  }
return NULL;
}

object sfs_eval_if(object obj){
    /*Verification des arguments de if*/
    object eval_expr1;
    object eval_expr2;
    object bool_expr;
    uint bool_value;

    DEBUT :

    if(obj->type != SFS_PAIR ||
        obj->this.pair.cdr->type != SFS_PAIR){
            WARNING_MSG("Mauvaise utilisation de if : if necessite une bool_expr et une ou deux eval_expr de la forme if( [Bool_expr] [eval_expr] ...)");
            return NULL;
        }
    /*if apriori correct */

    bool_expr = obj->this.pair.car;
    bool_value = sfs_eval_boolean(bool_expr);
    eval_expr1 = obj->this.pair.cdr->this.pair.car;

    if(obj->this.pair.cdr->this.pair.cdr == nil){
        if(bool_value == 1){
                if(is_if(eval_expr1)){
                        obj = eval_expr1->this.pair.cdr;
                        goto DEBUT;
                }
                else return sfs_eval(eval_expr1);
        }
        else return faux;
    }

    if(obj->this.pair.cdr->this.pair.cdr->type == SFS_PAIR){
        if(obj->this.pair.cdr->this.pair.cdr->this.pair.cdr != nil){
            WARNING_MSG("Mauvaise utilisation de if : trop d'arguments ");
            return NULL;
        }
        else{
            eval_expr2=obj->this.pair.cdr->this.pair.cdr->this.pair.car;
            if(bool_value == 1){
                if(is_if(eval_expr1)){
                    obj = eval_expr1->this.pair.cdr;
                    goto DEBUT;
                }
                else return sfs_eval(eval_expr1);
            }
            if(bool_value == 0){
                if(is_if(eval_expr2)){
                    obj = eval_expr2->this.pair.cdr;
                    goto DEBUT;
                }
                else return sfs_eval(eval_expr2);
            }
        }
    }

    WARNING_MSG("Arbre associé à if incorrect");
    return NULL;
}

object sfs_eval_cdr( object obj ){
  object pair;
  if(obj == nil){
    WARNING_MSG("ERREUR CDR : liste vide");
    return NULL;
  }
  pair=sfs_eval(obj->this.pair.car);
  if(pair == NULL) return NULL;
  if(pair == nil){
    WARNING_MSG("ERREUR CDR : liste vide");
    return NULL;
  }
  if(pair->type !=SFS_PAIR && pair->type != SFS_NORMAL_PAIR){
    WARNING_MSG("ERREUR CDR : l'objet n'est pas une pair");
    return NULL;
  }
  if(pair->this.pair.cdr == nil) return nil;
  return pair->this.pair.cdr;
}

object sfs_eval_car( object obj ){
  object pair;
  if(obj == nil){
    WARNING_MSG("ERREUR CAR : liste vide");
    return NULL;
  }
  pair=sfs_eval(obj->this.pair.car);
  if(pair == NULL) return NULL;
  if(pair == nil){
    WARNING_MSG("ERREUR CAR : liste vide");
    return NULL;
  }
  if(pair->type != SFS_PAIR && pair->type != SFS_NORMAL_PAIR){
    WARNING_MSG("ERREUR CAR : l'objet n'est pas une pair");
    return NULL;
  }
  return pair->this.pair.car;
}

object sfs_eval_cons(object obj){
  object arg1;
  object arg2;
  if(obj->type != SFS_PAIR || obj->this.pair.cdr->type != SFS_PAIR){
    WARNING_MSG("ERREUR CONS : cons necessite deux arguments");
    return NULL;
  }
  arg1=sfs_eval(obj->this.pair.car);
  if(arg1 == NULL) return NULL;

  if(obj->this.pair.cdr->this.pair.cdr != nil){
    WARNING_MSG("ERREUR CONS : trop d'arguments");
    return NULL;
  }
  arg2 = sfs_eval(obj->this.pair.cdr->this.pair.car);
  if(arg2 == NULL) return NULL;
  if(arg2->type == SFS_PAIR || arg2->type == SFS_NORMAL_PAIR) return make_pair(arg1,arg2);
  else return make_normal_pair(arg1,arg2);
}

object sfs_eval_list(object obj){
  object arg1;
  object top;
  object pair;
  if(obj == nil) return nil;
  pair = make_pair(nil,nil);
  top = pair;
  while(obj->type == SFS_PAIR){
    arg1 = sfs_eval(obj->this.pair.car);
    if(arg1 == NULL) return NULL;
    pair->this.pair.car = arg1;
    increase_count(arg1);
    obj = obj->this.pair.cdr;
    if(obj == nil) return top;
    else{
      pair->this.pair.cdr = make_pair(nil,nil);
      increase_count(pair->this.pair.cdr);
      pair = pair->this.pair.cdr;
    }
  }
  WARNING_MSG("ERREUR LIST");
  return NULL;
}

object sfs_eval_set_car(object obj){
  object variable;
  object valeur;
  if(obj->type!=SFS_PAIR || obj->this.pair.cdr->type!=SFS_PAIR ||
     obj->this.pair.cdr->this.pair.cdr!=nil){
    WARNING_MSG("Mauvaise utilisation de set_car!");
    return NULL;
  }

  if(obj->this.pair.car->type==SFS_FUNCTION){
       WARNING_MSG("ERREUR SET_CAR! : Impossible d'associer une valeur a ' %s ' , le nom est reserve",obj->this.pair.car->this.fct.name);
       return NULL;
  }

  if(obj->this.pair.car->type==SFS_SYMBOL){
    if(obj->this.pair.cdr->this.pair.car->type==SFS_STRING ||
       obj->this.pair.cdr->this.pair.car->type==SFS_CHARACTER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NUMBER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NORMAL_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PROCEDURE ||
       obj->this.pair.cdr->this.pair.car->type==SFS_FUNCTION ||
       obj->this.pair.cdr->this.pair.car->type==SFS_BOOLEAN ||
       obj->this.pair.cdr->this.pair.car->type==SFS_SYMBOL){
        variable=search_tabsymbol(obj->this.pair.car,tab_symbol);

        valeur = obj->this.pair.cdr->this.pair.car; /*par défaut on prend le car du cdr commm étant la valeur */

        if(valeur->type==SFS_SYMBOL){ /* si c'est un symbol on cherche sa valeur dans la table */

          valeur = search_tabsymbol(valeur,tab_symbol);
          if(valeur->type == SFS_VARIABLE) valeur = valeur->this.pair.cdr;
            else{
              WARNING_MSG("ERREUR SET_CAR! : la variable ' %s ' n'est pas definie",obj->this.pair.cdr->this.pair.car->this.symbol);
              return NULL;
            }
        }

        if(valeur->type==SFS_PAIR) valeur = sfs_eval(valeur); /* si c'est une paire on l'évalue */
        if(valeur == NULL) return NULL;

        if(variable==nil){ /* Si la variable n'est pas créée dans n'importe quel environnement mauvaise utilisation de set */
          WARNING_MSG("Mauvaise utilisation de set_car!: la variable n'existe dans aucun environnement");
          return NULL;
        }
        if(variable==NULL) return variable;
        else { /* Si la variable est déjà définit on supprime l'ancienne valeur et on la remplace par la nouvelle */
          if(variable->this.pair.cdr->type != SFS_PAIR && variable->this.pair.cdr->type != SFS_NORMAL_PAIR){
            WARNING_MSG("Mauvaise utilisation de set_car!: la variable '%s' n'est pas une paire",variable->this.pair.car->this.symbol);
            return NULL;
          }
          /*mémoire à optimiser ici */

          variable=variable->this.pair.cdr;
          decrease_count(variable->this.pair.car);
          increase_count(valeur);
          variable->this.pair.car=valeur;
        return nil;
        }
    }
    else{
      WARNING_MSG("Mauvais type de donnée");
      return NULL;
    }
  }
  else{
    WARNING_MSG("Le nom de la variable doit être un symbole");
    return NULL;
  }
return NULL;
}

object sfs_eval_set_cdr(object obj){
  object variable;
  object valeur;
  if(obj->type!=SFS_PAIR || obj->this.pair.cdr->type!=SFS_PAIR ||
     obj->this.pair.cdr->this.pair.cdr!=nil){
    WARNING_MSG("Mauvaise utilisation de set_cdr!");
    return NULL;
  }

  if(obj->this.pair.car->type==SFS_FUNCTION){
       WARNING_MSG("ERREUR SET_CDR! : Impossible d'associer une valeur a ' %s ' , le nom est reserve",obj->this.pair.car->this.fct.name);
       return NULL;
  }

  if(obj->this.pair.car->type==SFS_SYMBOL){
    if(obj->this.pair.cdr->this.pair.car->type==SFS_STRING ||
       obj->this.pair.cdr->this.pair.car->type==SFS_CHARACTER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NUMBER ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_NORMAL_PAIR ||
       obj->this.pair.cdr->this.pair.car->type==SFS_PROCEDURE ||
       obj->this.pair.cdr->this.pair.car->type==SFS_FUNCTION ||
       obj->this.pair.cdr->this.pair.car->type==SFS_BOOLEAN ||
       obj->this.pair.cdr->this.pair.car->type==SFS_SYMBOL){
        variable=search_tabsymbol(obj->this.pair.car,tab_symbol);

        valeur = obj->this.pair.cdr->this.pair.car; /*par défaut on prend le car du cdr commm étant la valeur */

        if(valeur->type==SFS_SYMBOL){ /* si c'est un symbol on cherche sa valeur dans la table */

          valeur = search_tabsymbol(valeur,tab_symbol);

          if(valeur->type == SFS_VARIABLE) valeur = valeur->this.pair.cdr;
            else{
              WARNING_MSG("ERREUR SET_CDR! : la variable ' %s ' n'est pas definie",obj->this.pair.cdr->this.pair.car->this.symbol);
              return NULL;
            }
        }

        if(valeur->type==SFS_PAIR) valeur = sfs_eval(valeur); /* si c'est une paire on l'évalue */
        if(valeur == NULL) return NULL;

        if(variable==nil){ /* Si la variable n'est pas créée dans n'importe quel environnement mauvaise utilisation de set */
          WARNING_MSG("Mauvaise utilisation de set_cdr!: la variable n'existe dans aucun environnement");
          return NULL;
        }
        if(variable==NULL) return variable;
        else { /* Si la variable est déjà définit on supprime l'ancienne valeur et on la remplace par la nouvelle */
          if(variable->this.pair.cdr->type != SFS_PAIR && variable->this.pair.cdr->type != SFS_NORMAL_PAIR){
            WARNING_MSG("Mauvaise utilisation de set_car!: la variable '%s' n'est pas une paire",variable->this.pair.car->this.symbol);
            return NULL;
          }
        /*mémoire à optimiser ici */
        variable=variable->this.pair.cdr;
        decrease_count(variable->this.pair.cdr);
        increase_count(valeur);
        variable->this.pair.cdr=valeur;
        return nil;
        }
    }
    else{
      WARNING_MSG("Mauvais type de donnée");
      return NULL;
    }
  }
  else{
    WARNING_MSG("Le nom de la variable doit être un symbole");
    return NULL;
  }
return NULL;
}

object sfs_eval_lambda(object obj){
  object formals;
  object body;
  if(obj->type != SFS_PAIR ||
    (obj->this.pair.car->type != SFS_PAIR && obj->this.pair.car != nil) ||
    obj->this.pair.cdr->type != SFS_PAIR  ){
      WARNING_MSG("ERROR LAMBDA: lambda s'utilise de la manière suivant :\n ( lambda (formals) (body) )");
       return NULL;
     }
  formals = obj->this.pair.car;
  body = obj->this.pair.cdr;
  return make_procedure(formals,body,tab_symbol);
}

object sfs_eval_begin(object obj){

	object output;
	object witness;
	output=obj;
	if(obj==nil)return obj;
	witness=output;
	while(witness!=nil){
		output=sfs_eval(witness->this.pair.car);
		if(output==NULL) return NULL;
		witness=witness->this.pair.cdr;
	}
	return output;
}

object sfs_eval_map(object obj){

  int refcountlist;
  int countlist;
  int count;
  int a = 1;
  object top_listoutput;
  object top_intermediate_list;
  object listoutput;
  object intermediate_list;
  object value_list;
  object ref_list;
  object value_fp;
  object main_travel;
  object fct_or_proc=sfs_eval(obj->this.pair.car);
  top_listoutput=nil;
  top_intermediate_list=nil;
  intermediate_list=nil;
  value_list=nil;
  listoutput=nil;
  main_travel=obj;
  refcountlist=0;
  countlist=0;
  if(main_travel == nil || (fct_or_proc->type != SFS_FUNCTION && fct_or_proc->type != SFS_PROCEDURE) ){
    WARNING_MSG("map doit être de la forme (map <proc> list-1 list-2 ... list-n)");
    return NULL;
  }
  main_travel=main_travel->this.pair.cdr;
  /* On va parcourir l'arbre pour vérifier que chaque liste (en vérifiant qu'on a bien des listes) rentrée par l'utilisateur
   fait la même taille; pour cela on compte les éléments de la première liste puis on vérifie que les autres listes ont le
   même nombre d'éléments que la première*/
   if(main_travel != nil){
     if(sfs_eval_ask_list(make_pair(main_travel->this.pair.car,nil))==faux){
       WARNING_MSG("A partir du deuxième argument, map attend une(des) liste(s)");
       return NULL;
     }
     else {
       decrease_count(main_travel->this.pair.car);
       main_travel->this.pair.car = sfs_eval(main_travel->this.pair.car);
       increase_count(main_travel->this.pair.car);
       ref_list=main_travel->this.pair.car;
       while(ref_list != nil){
         ref_list=ref_list->this.pair.cdr;
         refcountlist++;
       }
     }
     main_travel=main_travel->this.pair.cdr;
   }

  while(main_travel != nil){
    if(sfs_eval_ask_list(make_pair(main_travel->this.pair.car,nil))==faux){
      WARNING_MSG("A partir du deuxième argument, map attend une(des) liste(s)");
      return NULL;
    }
    else {
      decrease_count(main_travel->this.pair.car);
      main_travel->this.pair.car = sfs_eval(main_travel->this.pair.car);
      increase_count(main_travel->this.pair.car);
      ref_list=main_travel->this.pair.car;
      while(ref_list != nil){
        ref_list=ref_list->this.pair.cdr;
        countlist++;
      }
    }
    if(countlist != refcountlist){
       WARNING_MSG("Les listes doivent être de la même taille");
       return NULL;
     }
    main_travel=main_travel->this.pair.cdr;
    countlist=0;
  }
  /* On a donc des listes de la même taille à partir de cette ligne */
  main_travel=obj->this.pair.cdr;
  for (countlist = 0 ; countlist < refcountlist ; countlist++){ /* countlist = 0 correspond au premier élément de chaque liste,
                                                                   counlist = 1 au deuxième, etc...*/
    ref_list=main_travel->this.pair.car;
    for (count = 0 ; count < countlist ; count++){
      ref_list=ref_list->this.pair.cdr;
    }
    value_list=ref_list->this.pair.car;
    top_intermediate_list=make_pair(value_list,nil); /* on crée le haut de l'arbre de notre liste intermédiaire */
    intermediate_list=top_intermediate_list;
    main_travel=main_travel->this.pair.cdr;
    while(main_travel !=nil){
      ref_list=main_travel->this.pair.car;
      for (count=0 ; count < countlist ; count++){
        ref_list=ref_list->this.pair.cdr;
      }
      value_list=ref_list->this.pair.car;
      intermediate_list->this.pair.cdr=make_pair(value_list,nil);
      intermediate_list=intermediate_list->this.pair.cdr;
      main_travel=main_travel->this.pair.cdr;
    }
    /* A partir de là on a une (countlist+1)-ième liste intermédiaire constituée des (countlist+1)-ième éléments de chaque liste
      rentrées par l'utilisateur */
    if(fct_or_proc->type == SFS_PROCEDURE){
      if(a == 1) {
        value_fp = sfs_eval_procedure(fct_or_proc,top_intermediate_list);
        if(value_fp==NULL) return NULL;
        top_listoutput=make_pair( value_fp , nil );
        listoutput=top_listoutput;
        a--;
      } /* On a  crée le haut de la liste output (pour une procédure) */
      else {
        value_fp = sfs_eval_procedure(fct_or_proc,top_intermediate_list);
        if(value_fp==NULL) return NULL;
        decrease_count(listoutput->this.pair.cdr);
        listoutput->this.pair.cdr=make_pair( value_fp , nil );
        increase_count(listoutput->this.pair.cdr);
        listoutput=listoutput->this.pair.cdr;
      } /* on concatène les résultats d'évaluations de nos listes intermédiaires à top_listoutput ce qui donnera notre
       liste output (pour une procédure)*/

    }
    if(fct_or_proc->type == SFS_FUNCTION){
      if(a == 1) {
        value_fp = fct_or_proc->this.fct.ptr(top_intermediate_list);
        if( value_fp == NULL ) return NULL;
        top_listoutput=make_pair( value_fp , nil );
        listoutput=top_listoutput;
        a--;
      } /* On a  crée le haut de la liste output (pour une fonction) */
      else {
        value_fp = fct_or_proc->this.fct.ptr(top_intermediate_list);
        if( value_fp == NULL ) return NULL;
        decrease_count(listoutput->this.pair.cdr);
        listoutput->this.pair.cdr=make_pair( value_fp , nil );
        increase_count(listoutput->this.pair.cdr);
        listoutput=listoutput->this.pair.cdr;
      } /* on concatène les résultats d'évaluations de nos listes intermédiaires à top_listoutput ce qui donnera notre
       liste output (pour une fonction)*/

    }
    main_travel=obj->this.pair.cdr;
    top_intermediate_list=nil;
    intermediate_list=nil;
  }

return top_listoutput;
}
