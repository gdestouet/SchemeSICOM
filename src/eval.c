#include "eval.h"
#include "mem.h"

object init_tabsymbol(void){
  object metalevel;
  object default_syntax;
  object node;
  object userlevel;

  default_syntax = make_function( "define" , sfs_eval_define );
  node = make_pair(nil,default_syntax);

  default_syntax = make_function( "eqv?" , sfs_eval_eqv );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "char->integer" , sfs_eval_char_to_int );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "integer->char" , sfs_eval_int_to_char );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "number->str" , sfs_eval_number_to_str );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "str->number" , sfs_eval_str_to_number );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "symbol->str" , sfs_eval_symbol_to_str );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "str->symbol" , sfs_eval_str_to_symbol );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "set!" , sfs_eval_set );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "clear" , sfs_clear_mem );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "clearall" , sfs_all_clear_mem );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "clearenv" , sfs_clear_env );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "quit" , sfs_quit );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "set-car!" , sfs_eval_set_car );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "set-cdr!" , sfs_eval_set_cdr );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "let" , sfs_eval_let );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "let*" , sfs_eval_let_star );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "quote" , sfs_eval_quote );
  quote_function = default_syntax;
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "cdr" , sfs_eval_cdr);
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "car" , sfs_eval_car );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "cons" , sfs_eval_cons );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "if" , sfs_eval_if );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "or" , sfs_eval_or );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "and" , sfs_eval_and );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "not" , sfs_eval_not );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "lambda" , sfs_eval_lambda );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "null?" , sfs_eval_ask_null );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "boolean?" , sfs_eval_ask_boolean );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "symbol?" , sfs_eval_ask_symbol );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "integer?" , sfs_eval_ask_integer );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "complex?" , sfs_eval_ask_complex );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "map" , sfs_eval_map );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "real?" , sfs_eval_ask_real );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "string?" , sfs_eval_ask_string );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "pair?" , sfs_eval_ask_pair );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "list?" , sfs_eval_ask_list );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "char?" , sfs_eval_ask_char );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "list" , sfs_eval_list );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "=" , sfs_eval_boolean_operator_equal );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "<" , sfs_eval_boolean_operator_inf );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( ">" , sfs_eval_boolean_operator_sup );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "<=" , sfs_eval_boolean_operator_infeq );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( ">=" , sfs_eval_boolean_operator_supeq );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "+" , sfs_eval_operator_add );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "-" , sfs_eval_operator_sub );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "*" , sfs_eval_operator_multip );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "/" , sfs_eval_operator_divid );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "remainder" , sfs_eval_operator_remainder );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "modulo" , sfs_eval_operator_modulo );
  node = make_pair(node,default_syntax);

  default_syntax = make_function( "begin" , sfs_eval_begin );
  node = make_pair(node,default_syntax);

  metalevel = make_pair(nil,node);

  userlevel = add_level(metalevel);

  return userlevel;
}


uint loop_detector(object topvariable, object variable){
  object test = variable;
  while(test->this.pair.cdr->type == SFS_VARIABLE && test->this.pair.cdr != topvariable ) test = test->this.pair.cdr;
  if(test->this.pair.cdr == topvariable) return 1;
  else return 0;
}

object search_tabsymbol_environment(object symbol,object tabsymbol){
  object ptrenvi = tabsymbol->this.pair.cdr;
  while(ptrenvi != nil){

    if( ptrenvi->this.pair.cdr->type == SFS_FUNCTION){

      if( !strcmp(ptrenvi->this.pair.cdr->this.fct.name,symbol->this.symbol) ) return ptrenvi->this.pair.cdr;
      else ptrenvi = ptrenvi->this.pair.car;
    }
    else{
      if( !strcmp(ptrenvi->this.pair.cdr->this.pair.car->this.symbol,symbol->this.symbol) ) return ptrenvi->this.pair.cdr;
      else ptrenvi = ptrenvi->this.pair.car;
    }
  }
    /*Si le nom du symbole recherché n'est pas dans la table de symboles de l'environnement courant on renvoit nil */
  return nil;
}

object search_tabsymbol(object symbol,object tabsymbol){
  object ptr = tabsymbol;
  object variable;

  while(ptr != nil){
    variable=search_tabsymbol_environment(symbol,ptr);

    if(variable!=nil){
      if(ptr->this.pair.car==nil){
        WARNING_MSG("Impossible de modifier une variable réservée par le logiciel");
        return NULL;
      }
      else return variable;
    }
    ptr=ptr->this.pair.car;
  }
  /*Si le nom du symbole recherché n'est pas dans la table de symboles on renvoit nil */
  return nil;
}

object sfs_clear_mem(object obj){
  /*Fonction permettant de supprimer de la mémoire plusieurs variables */
  object ptr = tab_symbol;
  object ptrenvi;
  object node;
  object symbol;
  if(obj == NULL) return NULL;
  while(obj->type==SFS_PAIR){
    if(obj->this.pair.car->type != SFS_SYMBOL){
      WARNING_MSG("ERROR CLEAR : wrong arguments, symbols expected");
      return NULL;
    }
    symbol=obj->this.pair.car;
    while( ptr->this.pair.car != nil ){
      ptrenvi = ptr->this.pair.cdr;
      if( ptrenvi != nil ){
        if( !strcmp(ptrenvi->this.pair.cdr->this.pair.car->this.symbol,symbol->this.symbol) ){
          decrease_count(ptrenvi);
          ptr->this.pair.cdr=ptrenvi->this.pair.car;
          increase_count(ptr->this.pair.cdr);
          return nil;
        }
        node = ptrenvi;
        ptrenvi=ptrenvi->this.pair.car;
        while(ptrenvi != nil){
            if( !strcmp(ptrenvi->this.pair.cdr->this.pair.car->this.symbol,symbol->this.symbol) ){
              decrease_count(ptrenvi);
              node->this.pair.car=ptrenvi->this.pair.car;
              increase_count(node->this.pair.car);
              return nil;
            }
            node = ptrenvi;
            ptrenvi=ptrenvi->this.pair.car;
        }
      }
      ptr=ptr->this.pair.car;
    }
    obj=obj->this.pair.cdr;
  }
  return nil;
}

object sfs_all_clear_mem(){
  object ptr = tab_symbol;
  while(ptr->this.pair.car != nil){
    decrease_count(ptr);
    decrease_count(ptr->this.pair.cdr);
    ptr->this.pair.cdr = nil;
    tab_symbol = ptr;
    ptr = ptr->this.pair.car;
  }
  return nil;
}

object sfs_clear_env(){
  object ptr = tab_symbol;
  decrease_count(ptr);
  decrease_count(ptr->this.pair.cdr);
  ptr->this.pair.cdr = nil;
  /*si nous sommes sur le premier environnement utilisateur on ne met pas à jour tab_symbol sur le toplevel */
  if(ptr->this.pair.car->this.pair.car != nil ) tab_symbol = ptr;
  return nil;
}

object sfs_quit(){
  return quit;
}

object add_object_tabsymbol(object symbol,object tabsymbol){
    decrease_count(tabsymbol->this.pair.cdr);
    tabsymbol->this.pair.cdr = make_pair(tab_symbol->this.pair.cdr,symbol);
    increase_count(tabsymbol->this.pair.cdr);
    return symbol;
}

object add_level(object lowlevel){
  object newlevel = make_pair(lowlevel,nil);
  return newlevel;
}

object sfs_eval_procedure( object procedure , object value ){
  object current_envi = tab_symbol;
  object output;
  object top_var = make_pair(nil,nil);
  object var = top_var;
  object formals = procedure->this.proc.formals;
  object body = procedure->this.proc.body;
  /* evalue les parametres à partir de l'environnement courant */

  while(value!=nil){
    /*decrease_count(var->this.pair.car);*/
    if( SPECIAL_BINDING !=1 ){
      if(value->this.pair.car->this.pair.car == quote_function) var->this.pair.car = value->this.pair.car;
      else var->this.pair.car = sfs_eval(value->this.pair.car);
    }
    else var->this.pair.car = value->this.pair.car;
    increase_count(var->this.pair.car);
    if(!var->this.pair.car) return NULL;
    if(value->this.pair.cdr != nil){
      var->this.pair.cdr = make_pair(nil,nil);
      increase_count(var->this.pair.cdr);
      var = var->this.pair.cdr;
    }
    value=value->this.pair.cdr;
  }
  SPECIAL_BINDING=0;
  /*Modification de la variable globale tab_symbol pour creer un environnement a la procedure */
  tab_symbol = procedure->this.proc.env;
  tab_symbol = add_level(tab_symbol);
  /*Definition des variables utilises par la procedure dans son environnement*/
  while(formals!=nil && top_var!=nil){
    var = make_pair(formals->this.pair.car,make_pair(top_var->this.pair.car,nil));
    formals = formals->this.pair.cdr;
    top_var = top_var->this.pair.cdr;
    if(!sfs_eval_define(var)) return NULL;
  }
  /* Evaluation de la procedure */

  output = sfs_eval_begin(body);
  /*
  decrease_count(tab_symbol);
  sfs_garbage_collector();*/
  tab_symbol = current_envi;
  return output;
}

object sfs_eval( object input ) {
  object symbol;
  object value;
  if(input == NULL) return NULL;

  if(input->type == SFS_FUNCTION &&
      ( input->this.fct.ptr == sfs_all_clear_mem ||
        input->this.fct.ptr == sfs_clear_env ||
        input->this.fct.ptr == sfs_quit
      )
    ) return input->this.fct.ptr();

  if(input->type == SFS_FUNCTION) return input;

  if(input->type == SFS_NUMBER ||
     input->type == SFS_BOOLEAN ||
     input->type == SFS_CHARACTER ||
     input->type == SFS_STRING
   ) return input;

   if(input->type == SFS_NORMAL_PAIR){
     WARNING_MSG("Une paire doit etre utilise par une fonction");
     return NULL;
   }

  if(input->type == SFS_SYMBOL){
      symbol = search_tabsymbol(input,tab_symbol);
      if(symbol!=nil){
        if(symbol->type == SFS_VARIABLE) return symbol->this.pair.cdr;
      }
      else{
            WARNING_MSG("Symbole ' %s ' non defini", input->this.symbol);
            return NULL;
        }
    }

  if(input->type == SFS_PAIR){
    if(input->this.pair.car->type == SFS_FUNCTION) return input->this.pair.car->this.fct.ptr(input->this.pair.cdr);
    if(input->this.pair.car->type == SFS_PAIR || input->this.pair.car->type == SFS_SYMBOL){
      value = sfs_eval(input->this.pair.car);

      if(value == NULL) return NULL;
      if(value->type == SFS_FUNCTION){
        return value->this.fct.ptr(input->this.pair.cdr);
      }
      if(value->type == SFS_PROCEDURE){
        return sfs_eval_procedure(value,input->this.pair.cdr);
      }
      else return NULL;
    }
    else{
      WARNING_MSG("fonction non reconnue");
      return NULL;
    }
  }
  if(input->type == SFS_VARIABLE){
    return input->this.pair.cdr;
  }
  if(input != NULL) return input;
  else{
    WARNING_MSG("echec Evaluation");
    return NULL;
  }
 return NULL;
}
