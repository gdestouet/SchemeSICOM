;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(cdr '(a b c))
(cdr '((a) b c d))
(cdr '(1 . 2))
(cdr '(a ( b c )))
(cdr '((a b) (b c)))
