;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———

(car '(a b c))
(car '((a) b c d))
(car '(1 . 2))
(car '(a ( b c )))
(car '((a b) (b c)))
