;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(define a 1)
(< a 2)
(set! a 2)
(< 1 a)
(< 1 2)
(< -1 1)
(< -2 -1)
(< 1 -2)
(< -1 -2)
(< 2 1)
(< 1 -1)
