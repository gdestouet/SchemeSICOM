;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(list? '(4 5))
(list? (list 4 5))
(list? "abc")
(define a 5)
(list? a)
(list? #t)
(list? 4)
(list? ())
(list? #\a)
