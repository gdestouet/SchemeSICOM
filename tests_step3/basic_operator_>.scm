;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(define a 2)
(> a 1)
(> 2 1)
(> 1 -1)
(> -1 -2)
(> 1 2)
(> -2 -3)
(> -1 1)
(> -2 -1)
