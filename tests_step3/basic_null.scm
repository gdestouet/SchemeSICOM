;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(null? ())
(null? '(4 5))
(null? "abc")
(null? a)
(null? #t)
(null? 4)
(null? ())
(null? #\a)
