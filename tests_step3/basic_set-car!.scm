;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(define a '( 1 . 2 ) )
(set-car! a 3)
a
(set! a (list 1  2 ) )
(set-car! a 3)
a
(define j '(a b c))
(set-car! j 3)
j
(set! j '((a) b c d))
(set-car! j 3)
j
(set! j '(a ( b c )))
(set-car! j 3)
j
(set! j '((a b) (b c)))
(set-car! j 3)
j
