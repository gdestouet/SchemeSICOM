;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT="basic and"
;———
(boolean? #t)
(boolean? #f)
(boolean? 4)
(boolean? a)
(boolean? #\a)
(boolean? "abc")
(boolean? '(4 5))
(boolean? ())
