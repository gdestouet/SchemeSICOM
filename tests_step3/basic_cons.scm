;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———

(cons 'a '())
(cons '(a) '(b c d))
(cons "a" '(b c))
(cons 'a 3)
(cons '(a b) 'c)
