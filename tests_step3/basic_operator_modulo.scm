;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(define a 13)
(modulo a 4)
(set! a 4)
(modulo 13 a)
(modulo 13 4)
(modulo -13 4)
(modulo 13 -4)
(modulo -13 -4)
