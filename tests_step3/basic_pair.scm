;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;———
(pair? '(4 . 5))
(pair? (list 4 5))
(pair? "abc")
(define a 5)
(pair? a)
(pair? #t)
(pair? 4)
(pair? ())
(pair? #\a)
