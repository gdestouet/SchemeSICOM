;———
; TEST_RETURN_CODE=PASS
; TEST_COMMENT="basic quote"
;———
(quote abc)
'abc
(quote (+ 2 3 4))
'(+ 2 3 4)
(quote(quote abc))
''abc
(quote(quote (+ 2 3 4))
''(+ 2 3 4)
(quote(quote(quote(quote(quote(quote(quote(quote(quote(quote abc))))))))))
''''''''''abc
(quote(quote(quote(quote(quote(quote(quote(quote(quote(quote (+ 2 3 4)))))))))))
''''''''''(+ 2 3 4)
