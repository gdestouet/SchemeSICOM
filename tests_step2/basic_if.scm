;���
; TEST_RETURN_CODE=PASS
; TEST_COMMENT="Basic if"
;���
(if #t "predicat vrai" "predicat faux")
(if #f "predicat vrai" "predicat faux")
(if 5 "predicat vrai" "predicat faux")
(if 0 "predicat vrai" "predicat faux")
(if (and #t #t) "predicat vrai" "predicat faux")
(if (and #t #f) "predicat vrai" "predicat faux")
(if (or #t #t) "predicat vrai" "predicat faux")
(if (or #f #f) "predicat vrai" "predicat faux")
(if (< 1 2) "predicat vrai" "predicat faux")
(if (< 2 1) "predicat vrai" "predicat faux")
(if (<= 1 2) "predicat vrai" "predicat faux")
(if (<= 2 1) "predicat vrai" "predicat faux")
(if (> 2 1) "predicat vrai" "predicat faux")
(if (> 1 2) "predicat vrai" "predicat faux")
(if (>= 2 1) "predicat vrai" "predicat faux")
(if (>= 1 2) "predicat vrai" "predicat faux")
(if (= 1 1) "predicat vrai" "predicat faux")
(if (= 1 2) "predicat vrai" "predicat faux")



