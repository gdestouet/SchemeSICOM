#ifndef _READ_H_
#define _READ_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <ctype.h>
#include "object.h"
#include "eval.h"

enum {S_OK, S_KO, S_END};


object isintoplevel(object symbol);
uint number_type(char * input , uint * here);
uint type_real_complex(char * input , uint * here );
uint type_complex(char * input , uint * here );
uint is_signed(char input);
uint is_delimiter(char input);
uint jumpspace(char * input,uint * here);
uint jumpdigit(char * input,uint * here);

uint   sfs_get_sexpr( string input, FILE *fp );
object sfs_read( char *input, uint *here );
object sfs_read_atom( char *input, uint *here );
object sfs_read_number( char * input , uint * here );
num sfs_read_number_complex( char * input , uint * here );
object sfs_read_boolean( char * input , uint * here );
object sfs_read_char( char * input , uint * here );
object sfs_read_pair( char *stream , uint *i );
object sfs_read_symbol( char *input , uint *here );
object sfs_read_string( char *input, uint *here );
object sfs_read_quote_simplified ( char * input,uint * here);



#ifdef __cplusplus
}
#endif

#endif /* _READ_H_ */
