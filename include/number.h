#ifndef _NUMBER_H_
#define _NUMBER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "basic.h"
#include "limits.h"
#include "math.h"
#include "errno.h"


#define NUM_UNDEF    0x00
#define NUM_UINTEGER 0x01
#define NUM_INTEGER  0x02
#define NUM_REAL     0x03
#define NUM_COMPLEX  0x04
#define NUM_PINFTY   0x05
#define NUM_MINFTY   0x06

#define REAL_PART    0x01
#define COMPLEX_PART 0x02

typedef struct num_t {

    uint numtype;

    union {
        double        real;
        long          integer;
        unsigned long uinteger;

        struct complex {
          double RP;
          double IP;
        }cplx;
    } this;

} num;

#ifdef __cplusplus
}
#endif

#endif /* _NUMBER_H_ */
