#ifndef _EVAL_H_
#define _EVAL_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "object.h"
#include "read.h"
#include "primitive.h"

object init_tabsymbol(void);

object search_tabsymbol_environment(object symbol,object tabsymbol);

object search_tabsymbol(object symbol,object tabsymbol);

object add_object_tabsymbol(object symbol,object tabsymbol);

object add_level(object tabsymbol);

object sfs_clear_mem(object obj);
object sfs_all_clear_mem();
object sfs_clear_env();
object sfs_quit();

object sfs_eval_procedure(object procedure, object value);
object sfs_eval( object obj);

#ifdef __cplusplus
}
#endif

#endif /* _EVAL_H_ */
