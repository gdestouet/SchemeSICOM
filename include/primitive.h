#ifndef _PRIMITIVE_H_
#define _PRIMITIVE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "object.h"
#include "read.h"
#include "mem.h"
#include "eval.h"

uint SPECIAL_BINDING;
uint is_if(object eval_expr);

int sfs_eval_boolean(object obj);

double sfs_eval_number(num number,uint PART);

void convert_number(num * number , uint conversion_type);

object sfs_eval_operator_add(object obj);
object sfs_eval_operator_sub(object obj);
object sfs_eval_operator_multip(object obj);
object sfs_eval_operator_divid(object obj);
object sfs_eval_operator_remainder(object obj);
object sfs_eval_operator_modulo(object obj);
object sfs_eval_eqv(object obj);
object sfs_eval_boolean_operator_equal(object obj);
object sfs_eval_boolean_operator_inf(object obj);
object sfs_eval_boolean_operator_infeq(object obj);
object sfs_eval_boolean_operator_sup(object obj);
object sfs_eval_boolean_operator_supeq(object obj);
object sfs_eval_and(object obj);
object sfs_eval_or(object obj);
object sfs_eval_not(object obj);
object sfs_eval_ask_null(object obj);
object sfs_eval_ask_boolean(object obj);
object sfs_eval_ask_symbol(object obj);
object sfs_eval_ask_integer(object obj);
object sfs_eval_ask_complex(object obj);
object sfs_eval_ask_char(object obj);
object sfs_eval_ask_real(object obj);
object sfs_eval_ask_string(object obj);
object sfs_eval_ask_pair(object obj);
object sfs_eval_ask_list(object obj);
object sfs_eval_ask_procedure( object obj);
object sfs_eval_char_to_int(object obj);
object sfs_eval_int_to_char(object obj);
object sfs_eval_number_to_str(object obj);
object sfs_eval_str_to_number(object obj);
object sfs_eval_symbol_to_str(object obj);
object sfs_eval_str_to_symbol(object obj);
object sfs_eval_quote( object obj );
object sfs_eval_define(object obj);
object sfs_eval_set(object obj);
object sfs_eval_if(object obj);
object sfs_eval_cdr( object obj );
object sfs_eval_car( object obj );
object sfs_eval_cons( object obj );
object sfs_eval_list(object obj);
object sfs_eval_set_car(object obj);
object sfs_eval_set_cdr(object obj);
object sfs_eval_lambda(object obj);
object sfs_eval_begin(object obj);
object sfs_eval_let_star (  object obj );
object sfs_eval_let ( object obj );
object sfs_eval_map(object obj);




#ifdef __cplusplus
}
#endif

#endif /* _PRIMITIVE_H_ */
