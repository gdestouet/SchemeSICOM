#ifndef _OBJECT_H_
#define _OBJECT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "number.h"

typedef struct object_t {

  uint type;
  int ref_count;

  union {

    num              number;
    char             character;
    char *           string;
    char *           symbol;

    struct function {
      struct object_t * (*ptr)();
      char * name;
    } fct;

    struct procedure{
      struct object_t * formals;
      struct object_t * body;
      struct object_t * env;
    } proc;

    struct variable {
      struct object_t * valeur;
      char * name;
    } var;

    struct pair_t {
      struct object_t *car;
      struct object_t *cdr;
    } pair;

    struct object_t *special;

  } this;

} *object;

object make_object( uint type );
object make_nil( void );
object make_boolean( void );
object make_number(num number);
num make_number_num(double real_part, double complex_part, uint type);
object make_number_int( long int number );
object make_number_u_int( uint number );
object make_number_float( double number);
object make_char( char character );
object make_string( char *string );
object make_normal_pair( object car, object cdr );
object make_pair( object car, object cdr );
object make_symbol( char *symbol );
object make_number_minfty( void );
object make_number_pinfty( void );
object make_quit(void );
object make_procedure( object formals , object body , object env);
object make_variable( object symbol, object value );
object make_function( char * name, object (*ptr)() );
object make_syntax( char * symbol );
object make_operator( char * symbol );
object make_boolean_operator( char * symbol );


#define SFS_NUMBER       0x00
#define SFS_CHARACTER    0x01
#define SFS_STRING       0x02
#define SFS_PAIR         0x03
#define SFS_NIL          0x04
#define SFS_BOOLEAN      0x05
#define SFS_SYMBOL       0x06
#define SFS_FUNCTION     0x07
#define SFS_PROCEDURE    0x08
#define SFS_VARIABLE     0x09
#define SFS_NORMAL_PAIR  0x0A
#define SFS_QUIT         0x0B

extern object tab_symbol;
extern object nil;
extern object vrai;
extern object faux;
extern object quit;
extern object quote_function;

#ifdef __cplusplus
}
#endif

#endif /* _OBJECT_H_ */
