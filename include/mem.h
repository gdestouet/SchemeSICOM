#ifndef _MEM_H_
#define _MEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include "object.h"

object * MEMORY_TAB;
uint SIZE_MEMORY_TAB;

void debug_memory();
object * init_memory_tab();
void * sfs_malloc( size_t size );
void * sfs_calloc( size_t num ,size_t size );
void   sfs_free( void *ptr );
uint   sfs_garbage_collector();
void * resize();
void * add_ptr_memory_tab(void * ptr);
void * remove_ptr_memory_tab(void * ptr);
void clear_memory_tab();
uint sfs_free_object(object obj);
void decrease_count(object obj);
void increase_count(object obj);

#ifdef __cplusplus
}
#endif

#endif /* _MEM_H_ */
