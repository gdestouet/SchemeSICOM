;--
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;--
(let* ((x 5) (y 1)) (+ x y))
(define a 2)
(define b 3)
(let* ( (x 7) ( z (+ a b ) ) ) (* z x))
(let ((x 2) (y 3))
  (let*
  ( (x 7) (z (+ x y)) )
  (* z x)
  )
)
