;--
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;--
( define prc ( lambda ( x ) x ) )
( prc 8 )
( set! prc ( lambda ( x y ) ( * x y ) ) )
( prc (prc 2 3) 3 )
( define prcinv ( lambda ( x y ) ( prc x y ) ) )
( prcinv ( prc 2 3 ) 4 )
( set! prcinv ( lambda ( x y ) ( ( lambda ( x y ) ( * x y ) ) x y ) ) )
( prcinv 2 2 )
