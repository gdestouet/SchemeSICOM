;--
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;--
(map + '(1 2 3 4 5) '(6 7 8 9 10))
(map ( lambda (x y z) (* z (* x y) ) ) '( 1 2 ) '( 3 4 ) '( 5 6 ) )
(define star (lambda ( x y ) ( * x y ) ) )
(map star '(1 2) '(3 4) )
