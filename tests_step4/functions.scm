;--
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;--
(define  factorial  (lambda (n)
(if (= n 0)
1
(* n (factorial  (- n 1))))))
(factorial 20)
