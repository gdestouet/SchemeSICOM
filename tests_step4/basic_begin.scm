;--
; TEST_RETURN_CODE=PASS
; TEST_COMMENT=" "
;--
(define x 5)
(begin (define y 3) (set! x 4) (+ x y))
(begin (define star (lambda ( x y ) ( * x y ) ) ) (* x 2) )
